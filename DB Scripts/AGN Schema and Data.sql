USE [master]
GO
/****** Object:  Database [AGNTest]    Script Date: 12/4/2015 2:49:29 AM ******/
CREATE DATABASE [AGNTest]
GO
USE [AGNTest]
GO
/****** Object:  Table [dbo].[BookingAgent]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookingAgent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RNSBookingAgent_Id] [nvarchar](128) NOT NULL,
	[RNSLocationId] [nvarchar](500) NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](128) NULL,
	[Username] [nvarchar](128) NULL,
	[Password] [nvarchar](256) NULL,
	[CellNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_BookingAgent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailSent]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailSent](
	[Id] [nvarchar](128) NOT NULL,
	[Reservation_Id] [bigint] NULL,
	[EmailTemplate_Id] [int] NULL,
	[SentTo] [nvarchar](128) NULL,
	[Sent] [bit] NULL,
	[DatetimeSent] [datetime] NULL,
 CONSTRAINT [PK_EmailSent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[SendDays] [int] NULL,
	[RefWhen_Id] [int] NULL,
	[RefEvent_Id] [int] NULL,
	[RefReservationType_Id] [nvarchar](30) NULL,
	[ReservationGroup_Id] [int] NULL,
	[RNSBookingAgent_Id] [int] NULL,
	[FromEmail] [nvarchar](256) NULL,
	[BCC] [nvarchar](256) NULL,
	[Subject] [nvarchar](500) NULL,
	[Body] [nvarchar](max) NULL,
	[BalanceDue] [int] NULL,
 CONSTRAINT [PK_EmailTemplateDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Logs]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[DatetimeCreated] [datetime] NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RefEvent]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefEvent](
	[Id] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RefEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RefReservationType]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefReservationType](
	[Id] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RefReservationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RefSendTo]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefSendTo](
	[Id] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[SeqNum] [int] NULL,
 CONSTRAINT [PK_RefSendTo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RefWhen]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefWhen](
	[Id] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RefWhen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reservation]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OptOut] [bit] NOT NULL,
	[RNSReservation_Id] [bigint] NOT NULL,
	[RNSBookingAgent_Id] [nvarchar](128) NULL,
	[PropertyId] [int] NULL,
	[UnitNumber] [nvarchar](128) NULL,
	[GuestFirstName] [nvarchar](128) NULL,
	[GuestLastName] [nvarchar](128) NULL,
	[GuestEmail] [nvarchar](128) NULL,
	[GuestPhone] [nvarchar](128) NULL,
	[Type] [nvarchar](128) NULL,
	[InternalType] [nvarchar](128) NULL,
	[Arrival] [datetime] NULL,
	[Departure] [datetime] NULL,
	[BookDate] [datetime] NULL,
	[RNSLocation_Id] [nvarchar](128) NULL,
	[PaidInFull] [bit] NULL,
 CONSTRAINT [PK_Reservation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReservationGroup]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReservationGroup](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[RNSLocationId] [nvarchar](128) NULL,
 CONSTRAINT [PK_ReservationGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Setup]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setup](
	[Id] [int] NOT NULL,
	[SMTPServer] [nvarchar](256) NULL,
	[SMTPEmail] [nvarchar](256) NULL,
	[Port] [int] NULL,
	[Password] [nvarchar](256) NULL,
	[DefaultEmailAddress] [nvarchar](256) NULL,
	[DefaultMobileNumber] [nvarchar](128) NULL,
 CONSTRAINT [PK_Setup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TextSent]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TextSent](
	[Id] [nvarchar](128) NOT NULL,
	[Reservation_Id] [bigint] NULL,
	[TextTemplate_Id] [int] NULL,
	[SentTo] [nvarchar](128) NULL,
	[Sent] [bit] NULL,
	[DatetimeSent] [datetime] NULL,
 CONSTRAINT [PK_TextSent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TextTemplate]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TextTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[SendDays] [int] NULL,
	[RefWhen_Id] [int] NULL,
	[RefEvent_Id] [int] NULL,
	[RefReservationType_Id] [nvarchar](30) NULL,
	[ReservationGroup_Id] [int] NULL,
	[RNSBookingAgent_Id] [int] NULL,
	[MobileNumber] [nvarchar](50) NULL,
	[Body] [nvarchar](max) NULL,
	[BalanceDue] [int] NULL,
 CONSTRAINT [PK_TextTemplateDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UnitInfo]    Script Date: 12/4/2015 2:49:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnitInfo](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NULL,
	[Area] [nvarchar](50) NULL,
	[Sleeps] [int] NULL,
	[Bathrooms] [int] NULL,
	[Bedrooms] [int] NULL,
	[Street] [nvarchar](250) NULL,
	[City] [nvarchar](250) NULL,
	[State] [nvarchar](250) NULL,
	[PostalCode] [nvarchar](250) NULL,
	[Latitude] [nvarchar](250) NULL,
	[Longitude] [nvarchar](250) NULL,
	[IsActive] [bit] NULL,
	[PropertyNotes] [nvarchar](500) NULL,
	[PropertyURL] [nvarchar](500) NULL,
	[BookingURL] [nvarchar](500) NULL,
	[UnitNumber] [nvarchar](128) NULL,
 CONSTRAINT [PK_UnitInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[BookingAgent] ON 

INSERT [dbo].[BookingAgent] ([Id], [RNSBookingAgent_Id], [RNSLocationId], [Name], [Email], [Username], [Password], [CellNumber]) VALUES (4, N'7', N'1', N'test', N'test@admin.com', N'test@admin.com', N' test', N'639123460511')
INSERT [dbo].[BookingAgent] ([Id], [RNSBookingAgent_Id], [RNSLocationId], [Name], [Email], [Username], [Password], [CellNumber]) VALUES (5, N'20', N'1', N'Tester Chalmer', N'chalmer@zilverband.com', N'chalmer@zilverband.com', N'Tester 1234', NULL)
SET IDENTITY_INSERT [dbo].[BookingAgent] OFF
INSERT [dbo].[EmailSent] ([Id], [Reservation_Id], [EmailTemplate_Id], [SentTo], [Sent], [DatetimeSent]) VALUES (N'1', 741, NULL, N'chalmer@zilverband.com', 1, CAST(0x0000A56100000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[EmailTemplate] ON 

INSERT [dbo].[EmailTemplate] ([Id], [Name], [SendDays], [RefWhen_Id], [RefEvent_Id], [RefReservationType_Id], [ReservationGroup_Id], [RNSBookingAgent_Id], [FromEmail], [BCC], [Subject], [Body], [BalanceDue]) VALUES (10, N'test', 3, 2, 2, N'FR', 5, 5, N'chalmer@zilverband.com', NULL, N'test', N'<p>test body</p>', 2)
SET IDENTITY_INSERT [dbo].[EmailTemplate] OFF
INSERT [dbo].[RefEvent] ([Id], [Description]) VALUES (1, N'Booking Date')
INSERT [dbo].[RefEvent] ([Id], [Description]) VALUES (2, N'Arrival Date')
INSERT [dbo].[RefEvent] ([Id], [Description]) VALUES (3, N'Departure')
INSERT [dbo].[RefSendTo] ([Id], [Description], [SeqNum]) VALUES (1, N'By Reservation Number', 1)
INSERT [dbo].[RefSendTo] ([Id], [Description], [SeqNum]) VALUES (2, N'Arrivals Today', 2)
INSERT [dbo].[RefSendTo] ([Id], [Description], [SeqNum]) VALUES (3, N'Arrivals Tomorrow', 3)
INSERT [dbo].[RefSendTo] ([Id], [Description], [SeqNum]) VALUES (4, N'Departures Today', 4)
INSERT [dbo].[RefSendTo] ([Id], [Description], [SeqNum]) VALUES (5, N'Departures Tomorrow', 5)
INSERT [dbo].[RefSendTo] ([Id], [Description], [SeqNum]) VALUES (6, N'All In-House', 6)
INSERT [dbo].[RefWhen] ([Id], [Description]) VALUES (1, N'Before')
INSERT [dbo].[RefWhen] ([Id], [Description]) VALUES (2, N'After')
SET IDENTITY_INSERT [dbo].[Reservation] ON 

INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (1, 0, 750, NULL, 2, NULL, N'Chris', N'Walke', N'jeffp@rental-network.com', N'941-920-3555', N'R', N'GH', NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (2, 0, 743, N'7', 2, N'1605', N'NOVI (TEST)', N'NURAINI (TEST)', N'novinuraini3@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (3, 0, 723, N'7', 2, N'1605', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (4, 0, 734, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', CAST(0x0000A55B01567D0D AS DateTime), CAST(0x0000A56001567D0D AS DateTime), CAST(0x0000A55A01567D0D AS DateTime), N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (5, 0, 735, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (6, 0, 736, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (7, 0, 737, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (8, 0, 738, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (9, 0, 739, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (10, 0, 740, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (11, 0, 741, N'7', 7, N'1704', N'decce', N'tita', N'decce.tita@gmail.com', N'', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (12, 0, 696, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (13, 0, 699, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (14, 0, 700, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (15, 0, 702, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (16, 0, 703, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (17, 0, 704, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (18, 0, 705, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (19, 0, 706, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (20, 0, 698, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (21, 0, 707, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (22, 0, 708, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (23, 0, 709, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (24, 0, 710, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (25, 0, 716, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (26, 0, 718, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (27, 0, 719, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (28, 0, 728, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (29, 0, 711, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (30, 0, 712, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (31, 0, 713, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (32, 0, 714, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (33, 0, 715, N'7', 9, N'1608', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (34, 0, 721, N'7', 9, N'1608', N'Test', N'Test', N'jeffp@rental-network.com', N'1.888.337.6678', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (35, 0, 751, N'7', 23, N'APerfectPlc', N'Ali', N'Thomas', N'ali@rental-network.com', N'9417205445', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (36, 0, 720, N'12', 25, N'111235', N'Jeff', N'Pilson', N'jeffp@rental-network.com', N'9417467228105', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (37, 0, 746, N'7', 28, N'2BI', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
INSERT [dbo].[Reservation] ([Id], [OptOut], [RNSReservation_Id], [RNSBookingAgent_Id], [PropertyId], [UnitNumber], [GuestFirstName], [GuestLastName], [GuestEmail], [GuestPhone], [Type], [InternalType], [Arrival], [Departure], [BookDate], [RNSLocation_Id], [PaidInFull]) VALUES (38, 0, 747, N'7', 28, N'2BI', N'Luke', N'Lemon', N'll@test.com', N'4445556666', N'R', N'GC', NULL, NULL, NULL, N'1', 0)
SET IDENTITY_INSERT [dbo].[Reservation] OFF
INSERT [dbo].[Setup] ([Id], [SMTPServer], [SMTPEmail], [Port], [Password], [DefaultEmailAddress], [DefaultMobileNumber]) VALUES (1, N'smtp.mandrillapp.com', N'chabstester@gmail.com', 587, N'9P5PNHdhRCGpKM2Swqm04Q', N'chabstester@gmail.com', N'639123460367')
INSERT [dbo].[TextSent] ([Id], [Reservation_Id], [TextTemplate_Id], [SentTo], [Sent], [DatetimeSent]) VALUES (N'1', 750, NULL, N'chalmerfb@gmail.com', 1, CAST(0x0000A55F00000000 AS DateTime))
INSERT [dbo].[TextSent] ([Id], [Reservation_Id], [TextTemplate_Id], [SentTo], [Sent], [DatetimeSent]) VALUES (N'2', 721, NULL, N'chris@basebuildguys.com', 1, CAST(0x0000A56200000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[TextTemplate] ON 

INSERT [dbo].[TextTemplate] ([Id], [Name], [SendDays], [RefWhen_Id], [RefEvent_Id], [RefReservationType_Id], [ReservationGroup_Id], [RNSBookingAgent_Id], [MobileNumber], [Body], [BalanceDue]) VALUES (2, N'test email', 5, 2, 3, NULL, 1, 15, NULL, N'test email', 3)
SET IDENTITY_INSERT [dbo].[TextTemplate] OFF
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (2, N'Jeffs Place', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.

Hi Seth', N'House', N'', 4, 2, 2, N'123 Main St.', N'City Name', N'ST', N'12345-6789', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=2', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=2', N'1605')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (3, N'Plantations', N'Beautiful Lodge as the edge of the mountain.', N'', N'', 10, 4, 5, N'123 Main St.', N'City Name', N'ST', N'12345-6789', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=3', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=3', N'206')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (7, N'Ocean Gem', N'Beautiful Lodge as the edge of the beach.', N'', N'', 10, 4, 4, N'123 Main St.', N'City Name', N'ST', N'12345-6789', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=7', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=7', N'1704')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (9, N'The Suites', N'Beautiful Lodge as the edge of the mountain.', N'Condo', N'', 10, 2, 5, N'123 Main St.', N'City Name', N'ST', N'12345-6789', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=9', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=9', N'1608')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (16, N'Sun Palace', N'Beautiful Lodge as the edge of the mountain.', N'', N'', 4, 2, 2, N'2507 56th Ave E', N'Bradenton', N'FL', N'34203', N'27.4421062469482', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=16', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=16', N'AAA123')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (18, N'Beach Place', N'Beautiful Lodge as the edge of the mountain.', N'', N'', 4, 2, 2, N'2507 56th Ave E.', N'Bradenton', N'FL', N'34203', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=18', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=18', N'A101')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (20, N'Hannah''s Beach House', N'Beautiful Home backing up to a gorgeous lake view with boat access', N'House', N'', 10, 2, 3, N'2507 56th Ave E', N'Bradenton', N'FL', N'34203', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=20', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=20', N'Hann2507')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (22, N'Mandys River Run', N'Click the flags at the upper right-hand corner of the screen to change languages.', N'', N'', 8, 2, 4, N'6542  Gulf Dr.', N'Anna Maria', N'FL', N'34209', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=22', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=22', N'3333')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (23, N'A Perfect Place', N'This really is a Perfect Place!  Come and see, and enjoy the mountain air.

Hi Elizabeth !!!', N'Cabin', N'', 4, 2, 2, N'410 - 43rd St W', N'Bradenton', N'FL', N'34209', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=23', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=23', N'APerfectPlc')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (25, N'AAA Favorite Place', N'This place is a gem! <b>Really !!!</b>
<P>
A wonderful view over-looking the pool and into the mountains.
<P>
Come see for yourself !
<P>
You can use HTML to create links like this one that will open in another tab:
<P>

<a href="https://www.rental-network.com" target="_blank">Visit The RNS Web Site</a>', N'Condo', N'', 12, 4, 5, N'410 43rd St. West, unit J', N'Bradenton', N'FL', N'34209', N'27.496611', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=25', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=25', N'111235')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (28, N'Two Bedroom Island', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd St W', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=28', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=28', N'2BI')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (29, N'Two Bedroom Standard', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd St. W.', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=29', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=29', N'2BS')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (30, N'Two Bedroom Beachfront', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd St W', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=30', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=30', N'2BBF')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (31, N'Three Bedroom Townhome', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd St. W.', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=31', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=31', N'3BT1')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (32, N'Three Bedroom Beachfront', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd St W', N'Bradenton', N'FL', N'34209', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=32', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=32', N'3BBF')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (33, N'Three Bedroom Townhome', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd St. W.', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=33', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=33', N'3BT-2')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (34, N'Jeffs Place Beach Front', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.

Hey PJ 10/17/14', N'Condo', N'', 4, 2, 2, N'', N'', N'FL', N'', N'28.3819999694824', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=34', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=34', N'JeffsPlace')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (35, N'Ambers Condo', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.

Hi David', N'', N'', 4, 2, 2, N'415 Riverview Dr.', N'Jekyll', N'GA', N'31527', N'31.0675312', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=35', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=35', N'AW1')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (37, N'Sandys Place', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.

Hi Nicole', N'', N'', 4, 2, 2, N'410 43rd st W', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=37', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=37', N'SandysPlc')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (39, N'Matts House', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'', N'', N'FL', N'', N'28.3819999694824', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=39', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=39', N'Matt01')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (41, N'Primrose Path', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.

Hi Warren !!', N'Condo', N'', 4, 2, 2, N'410 43rd St West', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=41', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=41', N'PrimRose')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (52, N'The Simple Life', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'', N'Siesta Key', N'FL', N'', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=52', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=52', N'Village123')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (53, N'Party Time', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'', N'Siesta Key', N'FL', N'', N'30.637996', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=53', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=53', N'Village456')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (54, N'Hummingbird Cottage', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 10, 4, 5, N'701 Tropical Dr.', N'Bradenton', N'FL', N'34208', N'27.4934539794922', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=54', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=54', N'HumBird101')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (57, N'Amys BlackHills Home', N'This place is a gem! <b>Really !!!</b>

A wonderful view over-looking the pool and into the mountains.

Come see for yourself !', N'', N'', 12, 4, 5, N'', N'', N'SD', N'', N'28.4421062469482', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=57', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=57', N'Amy101')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (58, N'Sandy Toes Beach House', N'', N'', N'', 0, 0, 1, N'', N'', N'FL', N'', N'28.4930877685547', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=58', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=58', N'Sandra101')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (61, N'UNIT FOR COPY', N'A beautiful condo right on the beach.', N'', N'', 0, 0, 0, N'410 43rd St W', N'Bradenton', N'FL', N'34209', N'27.4969291687012', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=61', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=61', N'COPY1')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (68, N'Plantations', N'Beautiful Lodge as the edge of the mountain.', N'', N'', 10, 4, 5, N'123 Main St.', N'City Name', N'ST', N'12345-6789', N'30.637996', NULL, 0, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=68', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=68', N'z206')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (70, N'Unit in Loc 2', N'', N'', N'', 0, 0, 0, N'', N'Sarasota', N'FL', N'', N'27.272999207981442', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=70', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=70', N'101 Unit L2')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (72, N'Durango 101', N'A Beautiful Ski-in / Ski-out location.', N'', N'', 10, 0, 0, N'410 43rd Street West', N'Bradenton', N'FL', N'34208', N'27.496929', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=72', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=72', N'Durango101')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (73, N'Grande Cascades 6518', N'This place is a gem! <b>Really !!!</b>
<P>
A wonderful view over-looking the pool and into the mountains.
<P>
Come see for yourself !
<P>
You can use HTML to create links like this one that will open in another tab:
<P>

<a href="https://www.rental-network.com" target="_blank">Visit The RNS Web Site</a>', N'', N'', 12, 4, 5, N'6518 42nd Street E', N'Bradenton', N'FL', N'', N'27.4739608764648', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=73', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=73', N'Jennette2300')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (74, N'Bryce Rental', N'Beautiful Lodge at the edge of the mountain.

Just footsteps from historical Duval Street lies the pet friendly, picture-postcard perfect, two Queen Bedroom suite "Bahama Breeze".  Situated at the "Artsy" end of Duval Street in the heart of old town, this special location is centrally located to Key West''s gourmet restaurants, amenities, Hemmingway House, Little White House (where President Truman vacationed) and the picturesque Key West Lighthouse.

Within this cute conchy cottage you will find ample accommodations, complete with a large deck with your own private Spa and grill. Two spacious queen bedrooms offer you the comfort of home and more! The master bedroom features a TV/DVD as well as a large walk-in closet that houses a safe.', N'', N'', 4, 2, 2, N'410 43rd Street West', N'Bradenton', N'FL', N'34209', N'27.4967160075903', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=74', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=74', N'Bryce')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (75, N'Sand Dune Place 521', N'Beautiful Lodge as the edge of the mountain.', N'', N'', 10, 4, 5, N'410 43rd St W', N'Bradenton', N'FL', N'34209', N'27.4967160075903', NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=75', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=75', N'521 SD')
INSERT [dbo].[UnitInfo] ([Id], [Name], [Description], [Type], [Area], [Sleeps], [Bathrooms], [Bedrooms], [Street], [City], [State], [PostalCode], [Latitude], [Longitude], [IsActive], [PropertyNotes], [PropertyURL], [BookingURL], [UnitNumber]) VALUES (76, N'Sand Dune Place 522', N'Beautiful Lodge as the edge of the mountain.', N'', N'', 10, 4, 5, N'410 43rd Street W, Suite J', N'Bradenton', N'FL', N'34209', NULL, NULL, 1, N'', N'http://www.bestrentaldemoever.com/rns/search/propertydetail.aspx?id=76', N'http://www.bestrentaldemoever.com/rns/search/rentalterms.aspx?id=76', N'522 SD')
SET ANSI_PADDING ON

GO
/****** Object:  Index [BookingAgent_uq]    Script Date: 12/4/2015 2:49:30 AM ******/
ALTER TABLE [dbo].[BookingAgent] ADD  CONSTRAINT [BookingAgent_uq] UNIQUE NONCLUSTERED 
(
	[RNSBookingAgent_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Reservation_uq]    Script Date: 12/4/2015 2:49:30 AM ******/
ALTER TABLE [dbo].[Reservation] ADD  CONSTRAINT [Reservation_uq] UNIQUE NONCLUSTERED 
(
	[RNSReservation_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  CONSTRAINT [DF_EmailTemplateDetails_BCC]  DEFAULT ((0)) FOR [BCC]
GO
ALTER TABLE [dbo].[Logs] ADD  CONSTRAINT [DF_Logs_DatetimeCreated]  DEFAULT (getdate()) FOR [DatetimeCreated]
GO
ALTER TABLE [dbo].[Reservation] ADD  CONSTRAINT [DF_Reservation_OptOut]  DEFAULT ((0)) FOR [OptOut]
GO
ALTER TABLE [dbo].[EmailTemplate]  WITH CHECK ADD  CONSTRAINT [EmailTemplate_fk_RefEvent_Id] FOREIGN KEY([RefEvent_Id])
REFERENCES [dbo].[RefEvent] ([Id])
GO
ALTER TABLE [dbo].[EmailTemplate] CHECK CONSTRAINT [EmailTemplate_fk_RefEvent_Id]
GO
ALTER TABLE [dbo].[EmailTemplate]  WITH CHECK ADD  CONSTRAINT [EmailTemplate_fk_RefWhen_Id] FOREIGN KEY([RefWhen_Id])
REFERENCES [dbo].[RefWhen] ([Id])
GO
ALTER TABLE [dbo].[EmailTemplate] CHECK CONSTRAINT [EmailTemplate_fk_RefWhen_Id]
GO
ALTER TABLE [dbo].[TextTemplate]  WITH CHECK ADD  CONSTRAINT [TextTemplate_fk_RefEvent_Id] FOREIGN KEY([RefEvent_Id])
REFERENCES [dbo].[RefEvent] ([Id])
GO
ALTER TABLE [dbo].[TextTemplate] CHECK CONSTRAINT [TextTemplate_fk_RefEvent_Id]
GO
ALTER TABLE [dbo].[TextTemplate]  WITH CHECK ADD  CONSTRAINT [TextTemplate_fk_RefWhen_Id] FOREIGN KEY([RefWhen_Id])
REFERENCES [dbo].[RefWhen] ([Id])
GO
ALTER TABLE [dbo].[TextTemplate] CHECK CONSTRAINT [TextTemplate_fk_RefWhen_Id]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Either/Yes/No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmailTemplate', @level2type=N'COLUMN',@level2name=N'BalanceDue'
GO
