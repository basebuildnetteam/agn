﻿using ServiceRunner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron
{
    public class WinServiceInstaller : ServiceRunnerInstaller
    {
        protected override string ServiceName
        {
            get { return "AGNCron"; }
        }

        protected override string ServiceDescription
        {
            get { return "AGN Cron daily task"; }
        }

        protected override string ServiceDisplayName
        {
            get { return "AGN Cron"; }
        }

        protected override ServiceRunnerStartMode StartMode
        {
            get { return ServiceRunnerStartMode.Automatic; }
        }

        protected override ServiceRunnerAccount Account
        {
            get { return ServiceRunnerAccount.LocalSystem; }
        }
    }
}
