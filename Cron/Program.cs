﻿using ServiceRunner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron
{
    class Program
    {
        static void Main(string[] args)
        {
            Function process = new Function();
            //PART ONE: CHECK AND SEND EMAIL NOTIFICATIONS WHEN RESERVATIONS ARE DUE IN X DAYS
            

            //process.Test();
            //Console.ReadLine();

            var logFile = "c:\\temp\\logit.txt";
            var runner = new Runner("AGNCron", runAsConsole: true);
            runner.Run(args, 
                arguments =>
                {
                    //write your start code here
                    // . . . 
                    process.ProcessReservation();
                }, 
                () =>
                {
                    //write your stop code here
                    // . . . 
                });
        }
        
    }
}
