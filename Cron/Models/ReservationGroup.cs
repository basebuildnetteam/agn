﻿using Entity;
using RNSWebservice.WSPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Models
{
    public class ReservationGroup
    {

        public static void Save(List<ResvGroupInfo> resvGroupInfos)
        {
            AGNEntities db = new AGNEntities();
            foreach(ResvGroupInfo resGroup in resvGroupInfos)
            {
                if (db.ReservationGroups.Any(x => x.Id == resGroup.Id))
                {
                    continue;
                }
                Entity.ReservationGroup reservationGroup = new Entity.ReservationGroup()
                {
                    Id = resGroup.Id,
                    Name = resGroup.Name,
                    RNSLocationId = resGroup.RNSLocationId.ToString()
                };
                db.ReservationGroups.Add(reservationGroup);
            }
            db.SaveChanges();
        }
    }
}
