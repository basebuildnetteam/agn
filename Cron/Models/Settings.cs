﻿using Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Models
{
    public class Settings
    {
        AGNEntities _db = new AGNEntities();
        private Setup _email = null;
        private string _gTHYapiAccesskey;
        private int _gTHYapiMngrId;
        private string _rNSPortalWS1Accesskey;
        private int _rNSPortalWS1CoId;

        public Settings()
        {
            _email = new Setup();

            _email = _db.Setups.FirstOrDefault();
            _gTHYapiAccesskey = ConfigurationManager.AppSettings["GTHYapiAccesskey"];
            _gTHYapiMngrId = int.Parse(ConfigurationManager.AppSettings["GTHYapiMngrId"]);
            _rNSPortalWS1Accesskey = ConfigurationManager.AppSettings["RNSPortalWS1Accesskey"];
            _rNSPortalWS1CoId = int.Parse(ConfigurationManager.AppSettings["RNSPortalWS1CoId"]);
        }
        public Setup Email
        {
            get { return _email; }
        }
        public string GTHYapiAccesskey
        {
            get { return _gTHYapiAccesskey; }
        }
        public int GTHYapiMngrId
        {
            get { return _gTHYapiMngrId; }
        }
        public string RNSPortalWS1Accesskey
        {
            get { return _rNSPortalWS1Accesskey; }
        }
        public int RNSPortalWS1CoId
        {
            get { return _rNSPortalWS1CoId; }
        }
    }
}
