﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Utility
{
    public class Common
    {
        public static DateTime UTCToTimezone(DateTime utcDatetime)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZone"]);
            DateTime timeZoneDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcDatetime, timeZone);
            return timeZoneDateTime;
        }
    }
}
