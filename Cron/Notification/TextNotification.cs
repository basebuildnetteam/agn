﻿using Cron.Models;
using Cron.Utility;
using Entity;
using RNSWebservice.WSPortal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Notification
{
    public class TextNotification
    {
        AGNEntities _db = new AGNEntities();
        public void Notify(List<Entity.Reservation> agnReservations, string stopAlerMessage)
        {
            Settings settings = new Settings();
            Setup emailSettings = settings.Email;

            RNSPortalWS2 wsRNSPortalWS2 = new RNSPortalWS2();

            TextTemplate template = new TextTemplate();
            List<TextTemplate> textlTemplates = _db.TextTemplates.ToList();

            List<TextTemplate> textTemplatesFilter = _db.TextTemplates.ToList();
            List<TextTemplate> textTemplatesToUse = new List<TextTemplate>();

            
            string emailBody = "";
            //Email email = null;
            for (int i = 0; i < agnReservations.Count; i++)
            {

                long rnsReservationId = agnReservations[i].RNSReservation_Id;
                int rnsAgentId = int.Parse(agnReservations[i].RNSBookingAgent_Id);

                textTemplatesToUse = textlTemplates.Where(x => (x.RNSBookingAgent_Id == rnsAgentId || x.RNSBookingAgent_Id == 0)).ToList();
                foreach (TextTemplate textTemplate in textTemplatesToUse)
                {
                    var propDetail = wsRNSPortalWS2.GetPropertyDetails(settings.RNSPortalWS1Accesskey, settings.RNSPortalWS1CoId.ToString(), (Int32)agnReservations[i].PropertyId);
                    if (textTemplate.ReservationGroup_Id != 0)
                    {
                        if (propDetail.ReservationGroupId != textTemplate.ReservationGroup_Id)
                        {
                            continue;
                        }
                    }
                    // Create and execute raw SQL query.
                    StringBuilder query = new StringBuilder();
                    query.Append(@"  select R.GuestPhone
                      from Reservation R
                      inner join BookingAgent BA
                        on R.RNSBookingAgent_Id = BA.RNSBookingAgent_Id
                      ,TextTemplate ET
                        --on BA.RNSBookingAgent_Id = ET.RNSBookingAgent_Id
                      --inner join RefReservationType RRT
                      --  on ET.RefReservationType_Id = RRT.Id
                      --inner join ReservationGroup GRP
                      --  on R.RNSLocation_Id = GRP.RNSLocationId
                      where R.RNSReservation_Id = @@rnsReservationId -- = RNSReservation_Id                  
                      --and GRP.Id = ET.ReservationGroup_Id--ReservationGroup
                      and ET.Id = @@textTemplate_Id
                      @@RefEvent 
                      @@AgentId  
                      @@ReservationType --'GC' RefReservationType
                      @@BalanceDue
                    ");

                    string eventType = "";
                    if (textTemplate.RefEvent.Description == "Clean and Ready")
                    {
                        var unitStatus = wsRNSPortalWS2.GetUnitCleaningStatus(settings.RNSPortalWS1Accesskey, propDetail.UnitId.ToString());

                        query.Replace("@@RefEvent"
                            , @"and cast('@@currnetDate' as Datetime) == R.Arrival");
                    }
                    else
                    {
                        query.Replace("@@RefEvent"
                            , @"and cast('@@currnetDate' as Datetime) --Current Date
                            @@RefWhen -- < = Before or After
                            DateAdd(day,-@@Sendays,@@EventType) --  2 = Send Days, BookDate = Event");
                        if (textTemplate.RefEvent.Description == "Booking Date")
                        {
                            eventType = "R.BookDate";
                        }
                        else if (textTemplate.RefEvent.Description == "Arrival Date")
                        {
                            eventType = "R.Arrival";
                        }
                        else if (textTemplate.RefEvent.Description == "Departure")
                        {
                            eventType = "R.Departure";
                        }
                    }

                    if (textTemplate.BalanceDue == (int)Cron.Process.Reservation.Balance.Either)
                    {
                        query.Replace("@@BalanceDue", "");
                    }
                    else if (textTemplate.BalanceDue == (int)Cron.Process.Reservation.Balance.Yes)
                    {
                        query.Replace("@@BalanceDue", "And R.PaidInFull=1");
                    }
                    else
                    {
                        query.Replace("@@BalanceDue", "And R.PaidInFull=0");
                    }


                    if (textTemplate.RefReservationType_Id == "0" || textTemplate.RefReservationType_Id == null)
                    {
                        query.Replace("@@ReservationType", "");
                    }
                    else
                    {
                        query.Replace("@@ReservationType", "and R.InternalType = '" + textTemplate.RefReservationType_Id + "'");
                    }

                    if (textTemplate.RNSBookingAgent_Id == 0)
                    {
                        query.Replace("@@AgentId", "");
                    }
                    else
                    {
                        query.Replace("@@AgentId", "and BA.RNSBookingAgent_Id = " + textTemplate.RNSBookingAgent_Id);
                    }

                    query.Replace("@@AgentId", rnsAgentId.ToString());
                    query.Replace("@@currnetDate", Common.UTCToTimezone(DateTime.UtcNow).Date.ToString());
                    query.Replace("@@RefWhen", textTemplate.RefWhen.Description == "Before" ? "<" : ">");
                    query.Replace("@@Sendays", textTemplate.SendDays.ToString());
                    query.Replace("@@EventType", eventType);
                    query.Replace("@@rnsReservationId", agnReservations[i].RNSReservation_Id.ToString());
                    query.Replace("@@textTemplate_Id", textTemplate.Id.ToString());
                    //query.Replace("@@ReservationGroup", emailTemplate.ReservationGroup_Id.ToString());
                    //query.Replace("@@BalanceDue", "");
                    

                    string sendToMobile = _db.Database.SqlQuery<string>(query.ToString()).FirstOrDefault();

                    if (String.IsNullOrEmpty(sendToMobile))
                    {
                        continue;
                    }
                    else
                    {
                        DateTime utcDateNowFrom = Common.UTCToTimezone(DateTime.UtcNow).Date;
                        DateTime utcDateNowTo = Common.UTCToTimezone(DateTime.UtcNow.Date).AddHours(24).AddSeconds(-1);
                        bool isTextSentToday = _db.EmailSents.Any(x => (x.SentTo == sendToMobile && x.DatetimeSent >= utcDateNowFrom && x.DatetimeSent <= utcDateNowTo));
                        if (isTextSentToday == true)
                        {
                            continue;
                        }
                    }

                    //var unitInfo = _db.UnitInfoes.Where(x => x.UnitNumber == agnReservations[i].UnitNumber).FirstOrDefault();
                    int? propertyId = agnReservations[i].PropertyId;
                    var unitInfo = _db.UnitInfoes.Where(x => x.Id == propertyId).FirstOrDefault();

                    emailBody = textTemplate.Body;
                    //+ Environment.NewLine + Environment.NewLine
                    //+ stopAlerMessage;

                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(emailBody);
                    emailBody = doc.DocumentNode.InnerText;

                    StringBuilder body = new StringBuilder();
                    body.Append(emailBody);
                    body.Replace("[Guest First Name]", agnReservations[i].GuestFirstName);
                    body.Replace("[Guest Last Name]", agnReservations[i].GuestLastName);
                    body.Replace("[Reservation #]", agnReservations[i].ConfirmationNumber);
                    body.Replace("[Unit Number]", agnReservations[i].UnitNumber);
                    body.Replace("[Unit Name]", unitInfo.Name);
                    body.Replace("Unit Addres]", unitInfo.Street + ", " + unitInfo.City + " " + unitInfo.PostalCode);
                    //body.Replace("[Lock Code]", agnReservations[i].);
                    body.Replace("[Arrival Date]", agnReservations[i].Arrival.ToString());
                    body.Replace("[Departure Date]", agnReservations[i].Departure.ToString());


                    List<string> sendTo = new List<string>();

                    Text text = new Text();
                    if (ConfigurationManager.AppSettings["IsLiveMode"] == "0")
                    {
                        text.From = ConfigurationManager.AppSettings["SendTextFrom"];
                        sendTo.Add(ConfigurationManager.AppSettings["SendTextTo"]);
                    }
                    else
                    {
                        ////text.From = "+19412542875";
                        string agentId = rnsAgentId.ToString();
                        string textFrom = _db.BookingAgents.Where(x => x.RNSBookingAgent_Id == agentId).Select(x => x.CellNumber).FirstOrDefault();
                        if(String.IsNullOrWhiteSpace(textFrom)==true)
                        {
                            text.From = ConfigurationManager.AppSettings["SendTextFrom"]; 
                        }
                        else
                        {
                            text.From = textFrom;
                        }
                        //sendTo.Add(agnReservations[i].GuestPhone);
                        sendTo.Add(sendToMobile);

                    }

                    text.To = sendTo;
                    text.Message = body.ToString();
                    try
                    {
                        text.Send();
                    }
                    catch (Exception e)
                    {
                        Log log = new Log();
                        log.Message = e.Message;
                        log.DatetimeCreated = DateTime.Now;
                        _db.Logs.Add(log);
                        _db.SaveChanges();
                        throw e;
                    }

                    TextSent textSent = new TextSent()
                    {
                        Id = Guid.NewGuid().ToString(),
                        DatetimeSent = Common.UTCToTimezone(DateTime.UtcNow),
                        TextTemplate_Id = textTemplate.Id,
                        Reservation_Id = agnReservations[i].Id,
                        Sent = true,
                        SentTo = sendToMobile
                    };
                    _db.TextSents.Add(textSent);
                    _db.SaveChanges();
                }
            }
        }

        public void Notify(List<Entity.Reservation> agnReservations, SendEmailModel sendEmailModel, string stopAlerMessage)
        {

            Settings settings = new Settings();
            Setup emailSettings = settings.Email;

            string emailBody = "";
            int templateId = -1;
            if (String.IsNullOrWhiteSpace(sendEmailModel.emailtemplateid)==true || sendEmailModel.emailtemplateid.Replace("email_","").Replace("text_","") == "-1")
            {
                emailBody = sendEmailModel.body;
            }
            else
            {                
                
                if (sendEmailModel.emailtemplateid.ToString().Contains("email_"))
                {
                    templateId = int.Parse(sendEmailModel.emailtemplateid.ToString().Replace("email_", "").Trim());
                    var emailTemplate = _db.EmailTemplates.Where(x => x.Id == templateId).FirstOrDefault();
                    emailBody = emailTemplate.Body;
                }
                else
                {
                    templateId = int.Parse(sendEmailModel.emailtemplateid.ToString().Replace("text_", "").Trim());
                    var textTemplate = _db.TextTemplates.Where(x => x.Id == templateId).FirstOrDefault();
                    emailBody = textTemplate.Body;
                }
            }

            emailBody = emailBody;
                    //+ Environment.NewLine + Environment.NewLine
                    //+ stopAlerMessage;

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(emailBody);
            emailBody = doc.DocumentNode.InnerText;

            for (int i = 0; i < agnReservations.Count; i++)
            {

                long rnsReservationId = agnReservations[i].RNSReservation_Id;
                //int rnsAgentId = int.Parse(agnReservations[i].RNSBookingAgent_Id);
                //string unitNumber = agnReservations[i].UnitNumber;                
                int? propertyId = agnReservations[i].PropertyId;
                var unitInfo = _db.UnitInfoes.Where(x => x.Id == propertyId).FirstOrDefault();

                StringBuilder body = new StringBuilder();
                body.Append(emailBody);
                body.Replace("[Guest First Name]", agnReservations[i].GuestFirstName);
                body.Replace("[Guest Last Name]", agnReservations[i].GuestLastName);
                body.Replace("[Reservation #]", agnReservations[i].ConfirmationNumber);
                body.Replace("[Unit Number]", unitInfo.UnitNumber);
                body.Replace("[Unit Name]", unitInfo.Name);
                body.Replace("Unit Addres]", unitInfo.Street + ", " + unitInfo.City + " " + unitInfo.PostalCode);
                //body.Replace("[Lock Code]", agnReservations[i].);
                body.Replace("[Arrival Date]", agnReservations[i].Arrival.ToString());
                body.Replace("[Departure Date]", agnReservations[i].Departure.ToString());
                body.Replace("[Id]", agnReservations[i].Id.ToString());
                body.Replace("[ReservationId]", agnReservations[i].RNSReservation_Id.ToString());
                                              
                List<string> sendTo = new List<string>();


                Text text = new Text();
                if (ConfigurationManager.AppSettings["IsLiveMode"] == "0")
                {
                    text.From = ConfigurationManager.AppSettings["SendTextFrom"];
                    sendTo.Add(ConfigurationManager.AppSettings["SendTextTo"]);
                }
                else
                {
                    text.From = sendEmailModel.sendFromText;
                    sendTo.Add(agnReservations[i].GuestPhone);

                }

                text.To = sendTo;
                text.Message = body.ToString();
                try
                {
                    text.Send();
                }
                catch (Exception e)
                {
                    Log log = new Log();
                    log.Message = e.Message + ". " + e.InnerException.ToString();
                    log.DatetimeCreated = Common.UTCToTimezone(DateTime.UtcNow);
                    _db.Logs.Add(log);
                    _db.SaveChanges();
                    throw e;
                }

                TextSent textSent = new TextSent()
                {
                    Id = Guid.NewGuid().ToString(),
                    DatetimeSent =  Common.UTCToTimezone(DateTime.UtcNow),
                    TextTemplate_Id = templateId,
                    Reservation_Id = agnReservations[i].Id,
                    Sent = true,
                    SentTo = agnReservations[i].GuestPhone
                };
                _db.TextSents.Add(textSent);
            }
            _db.SaveChanges();

        }

    }
}
