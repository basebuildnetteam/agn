﻿using Cron.Models;
using Cron.Utility;
using Entity;
using RNSWebservice.WSPortal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Notification
{
    public class EmailNotification
    {
        AGNEntities _db = new AGNEntities();
        public void Notify(List<Entity.Reservation> agnReservations, string stopAlerMessage)
        {

            Settings settings = new Settings();
            Setup emailSettings = settings.Email;

            RNSPortalWS2 wsRNSPortalWS1 = new RNSPortalWS2();

            EmailTemplate template = new EmailTemplate();
            List<EmailTemplate> emailTemplates = _db.EmailTemplates.ToList();

            List<EmailTemplate> emailTemplatesFilter = _db.EmailTemplates.ToList();
            List<EmailTemplate> emailTemplatesToUse = new List<EmailTemplate>();

            Email email = null;
            for (int i = 0; i < agnReservations.Count; i++)
            {

                long rnsReservationId = agnReservations[i].RNSReservation_Id;
                int rnsAgentId = int.Parse(agnReservations[i].RNSBookingAgent_Id);

                emailTemplatesToUse = emailTemplates.Where(x => (x.RNSBookingAgent_Id == rnsAgentId || x.RNSBookingAgent_Id == 0)).ToList();
                foreach (EmailTemplate emailTemplate in emailTemplatesToUse)
                {
                    var propDetail = wsRNSPortalWS1.GetPropertyDetails(settings.RNSPortalWS1Accesskey, settings.RNSPortalWS1CoId.ToString(), (Int32)agnReservations[i].PropertyId);
                    if (emailTemplate.ReservationGroup_Id != 0)
                    {
                        if (propDetail.ReservationGroupId != emailTemplate.ReservationGroup_Id)
                        {
                            continue;
                        }
                    }
                    // Create and execute raw SQL query.
                    StringBuilder query = new StringBuilder();
                    query.Append(@"  select R.GuestEmail
                      from Reservation R
                      inner join BookingAgent BA
                        on R.RNSBookingAgent_Id = BA.RNSBookingAgent_Id
                      ,EmailTemplate ET
                        --on BA.RNSBookingAgent_Id = ET.RNSBookingAgent_Id
                      --inner join RefReservationType RRT
                      --  on ET.RefReservationType_Id = RRT.Id
                      --inner join ReservationGroup GRP
                      --  on R.RNSLocation_Id = GRP.RNSLocationId
                      where R.RNSReservation_Id = @@rnsReservationId -- = RNSReservation_Id
                      --and GRP.Id = ET.ReservationGroup_Id--ReservationGroup
                      and ET.Id = @@EmailTemplate_Id
                      @@RefEvent 
                      @@AgentId  
                      @@ReservationType --'GC' RefReservationType
                      @@BalanceDue
                    ");
                        
                    
                    string eventType = "";
                    if (emailTemplate.RefEvent.Description == "Clean and Ready")
                    {
                        query.Replace("@@RefEvent"
                            , @"and cast('@@currnetDate' as Datetime) == R.Arrival");
                    }
                    else
                    {
                        query.Replace("@@RefEvent"
                            , @"and cast('@@currnetDate' as Datetime) --Current Date
                            @@RefWhen -- < = Before or After
                            DateAdd(day,-@@Sendays,@@EventType) --  2 = Send Days, BookDate = Event");
                        if (emailTemplate.RefEvent.Description == "Booking Date")
                        {
                            eventType = "R.BookDate";
                        }
                        else if (emailTemplate.RefEvent.Description == "Arrival Date")
                        {
                            eventType = "R.Arrival";
                        }
                        else if(emailTemplate.RefEvent.Description == "Departure")
                        {
                            eventType = "R.Departure";
                        }
                    }
                    

                    
                    
                    if(emailTemplate.BalanceDue == (int)Cron.Process.Reservation.Balance.Either)
                    {
                        query.Replace("@@BalanceDue", "");
                    }
                    else if (emailTemplate.BalanceDue == (int)Cron.Process.Reservation.Balance.Yes)
                    {
                        query.Replace("@@BalanceDue", "And R.PaidInFull=1");
                    }
                    else
                    {
                        query.Replace("@@BalanceDue", "And R.PaidInFull=0");
                    }

                    if (emailTemplate.RefReservationType_Id == "0" || emailTemplate.RefReservationType_Id==null)
                    {
                        query.Replace("@@ReservationType", "");
                    }
                    else
                    {
                        query.Replace("@@ReservationType", "and R.InternalType = '" + emailTemplate.RefReservationType_Id + "'");
                    }
                    if (emailTemplate.RNSBookingAgent_Id == 0)
                    {
                        query.Replace("@@AgentId", "");
                    }
                    else
                    {
                        query.Replace("@@AgentId", "and BA.RNSBookingAgent_Id = " + emailTemplate.RNSBookingAgent_Id);
                    }

                    query.Replace("@@AgentId", rnsAgentId.ToString());


                    DateTime timezoneDateTime = Common.UTCToTimezone(DateTime.UtcNow);

                    query.Replace("@@currnetDate", timezoneDateTime.Date.ToString());
                    query.Replace("@@RefWhen", emailTemplate.RefWhen.Description == "Before" ? "<" : ">");
                    query.Replace("@@Sendays", emailTemplate.SendDays.ToString());
                    query.Replace("@@EventType", eventType);
                    query.Replace("@@rnsReservationId", agnReservations[i].RNSReservation_Id.ToString());
                    query.Replace("@@EmailTemplate_Id", emailTemplate.Id.ToString());
                    //query.Replace("@@ReservationGroup", emailTemplate.ReservationGroup_Id.ToString());
                    //query.Replace("@@BalanceDue", "");


                    string sendToEmail = _db.Database.SqlQuery<string>(query.ToString()).FirstOrDefault();


                    if(String.IsNullOrEmpty(sendToEmail))
                    {
                        continue;
                    }
                    else
                    {
                        DateTime utcDateNowFrom =Common.UTCToTimezone(DateTime.UtcNow).Date;
                        DateTime utcDateNowTo = Common.UTCToTimezone(DateTime.UtcNow).Date.AddHours(24).AddSeconds(-1);
                        bool isEmailSentToday = _db.EmailSents.Any(x => (x.SentTo == sendToEmail && x.DatetimeSent >= utcDateNowFrom && x.DatetimeSent <=utcDateNowTo));
                        if (isEmailSentToday == true)
                        {
                            continue;
                        }
                    }

                    //var unitInfo = _db.UnitInfoes.Where(x => x.UnitNumber == agnReservations[i].UnitNumber).FirstOrDefault();

                    int? propertyId = agnReservations[i].PropertyId;
                    var unitInfo = _db.UnitInfoes.Where(x => x.Id == propertyId).FirstOrDefault();

                    string emailBody = emailTemplate.Body;
                        //+ Environment.NewLine + Environment.NewLine
                        //+ stopAlerMessage;

                    //HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    //doc.LoadHtml(emailBody);
                    //emailBody = doc.DocumentNode.InnerText;

                    StringBuilder body = new StringBuilder();
                    body.Append(emailBody);
                    body.Replace("[Guest First Name]", agnReservations[i].GuestFirstName);
                    body.Replace("[Guest Last Name]", agnReservations[i].GuestLastName);
                    body.Replace("[Reservation #]", agnReservations[i].ConfirmationNumber);
                    body.Replace("[Unit Number]", unitInfo.UnitNumber);
                    body.Replace("[Unit Name]", unitInfo.Name);
                    body.Replace("Unit Addres]", unitInfo.Street + ", " + unitInfo.City + " " + unitInfo.PostalCode);
                    //body.Replace("[Lock Code]", agnReservations[i].);
                    body.Replace("[Arrival Date]", agnReservations[i].Arrival.ToString());
                    body.Replace("[Departure Date]", agnReservations[i].Departure.ToString());


                    email = new Email();
                    if (ConfigurationManager.AppSettings["IsLiveMode"] == "0")
                    {
                        email.From = new MailAddress(emailSettings.DefaultEmailAddress);
                        email.To = new MailAddress(ConfigurationManager.AppSettings["SendEmailTo"]);
                    }
                    else
                    {
                        email.From = new MailAddress(emailSettings.DefaultEmailAddress);
                        //email.To = new MailAddress(agnReservations[i].GuestEmail);   //User in live deployment 
                        email.To = new MailAddress(sendToEmail);

                    }
                    if (String.IsNullOrWhiteSpace(emailTemplate.BCC) == true || emailTemplate.BCC=="0")
                    {
                        string bcc = _db.BookingAgents.Where(x => x.RNSBookingAgent_Id == rnsAgentId.ToString()).Select(x => x.Email).FirstOrDefault();
                        email.Bcc = new MailAddress(bcc);    //always not empty 
                    }
                    email.Subject = emailTemplate.Subject;
                    email.Body = body.ToString();
                    try
                    {
                        email.Send();
                    }
                    catch (Exception e)
                    {
                        Log log = new Log();
                        log.Message = e.Message;
                        log.DatetimeCreated = DateTime.Now;
                        _db.Logs.Add(log);
                        _db.SaveChanges();
                        throw e;
                    }
                    EmailSent emailSent = new EmailSent()
                    {
                        Id = Guid.NewGuid().ToString(),
                        DatetimeSent = Common.UTCToTimezone(DateTime.UtcNow).Date,
                        EmailTemplate_Id = emailTemplate.Id,
                        Reservation_Id = agnReservations[i].Id,
                        Sent = true,
                        SentTo = sendToEmail                          
                    };
                    _db.EmailSents.Add(emailSent);
                    _db.SaveChanges();
                }
            }
        }

        public void Notify(List<Entity.Reservation> agnReservations, SendEmailModel sendEmailModel, string stopAlerMessage)
        {
            try
            {
                Settings settings = new Settings();
                Setup emailSettings = settings.Email;
                string emailBody = "";
                int templateId = -1;
                if (sendEmailModel.emailtemplateid == null || sendEmailModel.emailtemplateid.Replace("email_","").Replace("text_","") == "-1")
                {
                    emailBody = sendEmailModel.body;
                }
                else
                {
                    if(sendEmailModel.emailtemplateid.ToString().Contains("email_"))
                    {
                        templateId = int.Parse(sendEmailModel.emailtemplateid.ToString().Replace("email_", "").Trim());
                        var emailTemplate = _db.EmailTemplates.Where(x => x.Id == templateId).FirstOrDefault();
                        emailBody = emailTemplate.Body;
                    }
                    else
                    {
                        templateId = int.Parse(sendEmailModel.emailtemplateid.ToString().Replace("text_", "").Trim());
                        var textTemplate = _db.TextTemplates.Where(x => x.Id == templateId).FirstOrDefault();
                        emailBody = textTemplate.Body;
                    }
                }
                emailBody = emailBody;
                        //+ Environment.NewLine + Environment.NewLine
                        //+ stopAlerMessage;

                //HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                //doc.LoadHtml(emailBody);
                //emailBody = doc.DocumentNode.InnerText;

                for (int i = 0; i < agnReservations.Count; i++)
                {

                    long rnsReservationId = agnReservations[i].RNSReservation_Id;
                    //int rnsAgentId = int.Parse(agnReservations[i].RNSBookingAgent_Id);
                    //string unitNumber = agnReservations[i].UnitNumber; 
                    int? propertyId = agnReservations[i].PropertyId;
                    var unitInfo = _db.UnitInfoes.Where(x => x.Id == propertyId).FirstOrDefault();


                    StringBuilder body = new StringBuilder();
                    body.Append(emailBody);
                    body.Replace("[Guest First Name]", agnReservations[i].GuestFirstName);
                    body.Replace("[Guest Last Name]", agnReservations[i].GuestLastName);
                    body.Replace("[Reservation #]", agnReservations[i].ConfirmationNumber);
                    body.Replace("[Unit Number]", unitInfo.UnitNumber);
                    body.Replace("[Unit Name]", unitInfo.Name);
                    body.Replace("Unit Addres]", unitInfo.Street + ", " + unitInfo.City + " " + unitInfo.PostalCode);
                    //body.Replace("[Lock Code]", agnReservations[i].);
                    body.Replace("[Arrival Date]", agnReservations[i].Arrival.ToString());
                    body.Replace("[Departure Date]", agnReservations[i].Departure.ToString());
                    body.Replace("[Id]", agnReservations[i].Id.ToString());
                    body.Replace("[ReservationId]", agnReservations[i].RNSReservation_Id.ToString());


                    Email email = new Email();
                    if (ConfigurationManager.AppSettings["IsLiveMode"] == "0")
                    {
                        email.From = new MailAddress(ConfigurationManager.AppSettings["SendEmailFrom"]);
                        email.To = new MailAddress(ConfigurationManager.AppSettings["SendEmailTo"]);
                    }
                    else
                    {
                        email.From = new MailAddress(sendEmailModel.sendFromEmail);
                        email.To = new MailAddress(agnReservations[i].GuestEmail);   //User in live deployment 
                    }

                    if (String.IsNullOrWhiteSpace(sendEmailModel.bcc) == true)
                    {
                        string bcc = _db.BookingAgents.Where(x => x.RNSBookingAgent_Id == agnReservations[i].RNSBookingAgent_Id).Select(x => x.Email).FirstOrDefault();
                        email.Bcc = new MailAddress(bcc);    //always not empty 
                    }
                    email.Bcc = new MailAddress(sendEmailModel.bcc);    //always not empty                   
                    email.Subject = sendEmailModel.subject;
                    email.Body = body.ToString();
                    try
                    {
                        email.Send();
                    }
                    catch
                    {
                        throw;
                    }


                    EmailSent emailSent = new EmailSent()
                    {
                        Id = Guid.NewGuid().ToString(),
                        DatetimeSent = Common.UTCToTimezone(DateTime.UtcNow),
                        EmailTemplate_Id = templateId,
                        Reservation_Id = agnReservations[i].Id,
                        Sent = true,
                        SentTo = agnReservations[i].GuestEmail
                    };
                    _db.EmailSents.Add(emailSent);
                }
                _db.SaveChanges();
            }

            catch (Exception e)
            {
                Log log = new Log();
                log.Message = e.Message + ". " + e.InnerException.ToString();
                log.DatetimeCreated = DateTime.Now;
                _db.Logs.Add(log);
                _db.SaveChanges();
                throw e;
            }
        }
       
    }
}
