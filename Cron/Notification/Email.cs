﻿using Cron.Models;
using Entity;
using Mandrill.Requests.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Notification
{
    public class Email
    {
        public MailAddress From { get; set; }
        public MailAddress To { get; set; }
        public MailAddress Bcc { get; set; }
        public MailAddress Cc { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }

        public void Send()
        {
            if (ConfigurationManager.AppSettings["SendEmailBy"].ToLower() == "smtp")
            {
                SendBySmtp();
            }
            else
            {
                SendByMandrill();
            }
        }
        public void SendBySmtp()
        {
            try
            {
                Settings settings = new Settings();
                Setup emailSettings = settings.Email;

                MailMessage mailMsg = new MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Port = (int)emailSettings.Port;
                smtpClient.Host = emailSettings.SMTPServer; // "mail.basebuildguys.com";
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpClient.UseDefaultCredentials = false;
                smtpClient.EnableSsl = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(emailSettings.SMTPEmail, emailSettings.Password);
                mailMsg.To.Add(To);
                if (Cc != null) { mailMsg.CC.Add(Cc); }
                if (Bcc != null) { mailMsg.Bcc.Add(Bcc); }

                // From
                mailMsg.From = From;
                mailMsg.Subject = Subject;
                mailMsg.Body = Body;
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(Body, null, MediaTypeNames.Text.Html));
                mailMsg.IsBodyHtml = true;
                smtpClient.Send(mailMsg);

                /*
                MailMessage mailMsg = new MailMessage();

                // To
                //mailMsg.To.Add(new MailAddress("to@example.com", "To Name"));
                mailMsg.To.Add(To);
                if (Cc != null) { mailMsg.CC.Add(Cc); }
                if (Bcc != null) { mailMsg.Bcc.Add(Bcc); }

                // From
                //mailMsg.From = new MailAddress("from@example.com", "From Name");
                mailMsg.From = From;
                // Subject and multipart/alternative Body
                mailMsg.Subject = Subject;
                //string text = "<html><body>" + Body + "</body> </html>"; 
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(Body, null, MediaTypeNames.Text.Html));
                mailMsg.IsBodyHtml = true;

                // Initialized SmtpClient and send
                //SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", Convert.ToInt32(587));
                SmtpClient smtpClient = new SmtpClient("mail.basebuildguys.com", Convert.ToInt32(25));
                //client.UseDefaultCredentials = true;
                //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("chabstester@gmail.com", "test1234!");
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("agn@basebuildguys.com", "test123!");
                smtpClient.Credentials = credentials;
                //smtpClient.EnableSsl = true;
                //smtpClient.Port = 587;
                smtpClient.EnableSsl = false;
                smtpClient.Port = 25;
                smtpClient.Send(mailMsg);
                */
            }
            catch
            {
                throw;
            }
        }

        public void SendByMandrill()
        {
            try
            {
                //Email sourceEmail = new Email();
                EmailByMandrill emailByMandrill = new EmailByMandrill();
                bool result = emailByMandrill.Send(From.Address, To.Address, Bcc.Address, Subject, Body);
            }
            catch
            {
                throw;
            }
        }

    }
    public class EmailByMandrill
    {

        public bool Send(string sendFromEmail, string sendToEmail, string bcc, string subject, string body)
        {
            bool isSent = false;
            try
            {
                List<Mandrill.Models.EmailAddress> emails = new List<Mandrill.Models.EmailAddress>();
                //foreach(string sendToEmail in sendToEmails)
                //{
                //emails.Add(new Mandrill.Models.EmailAddress(sendToEmail));

                //}
                var request = new SendMessageRequest(
                   new Mandrill.Models.EmailMessage
                   {
                       FromEmail = sendFromEmail, //"chabstester@gmail.com",
                       Html = "<html><body>" + body + "</body></html>",
                       Subject = subject,
                       To = new Mandrill.Models.EmailAddress[] { 
                        new Mandrill.Models.EmailAddress(sendToEmail) //actual guest email
                        //,new Mandrill.Models.EmailAddress("JeffP@rental-network.com", "Jeff Pilson")
                       },
                       BccAddress = bcc


                   });
                string mandrillApiKey = ConfigurationManager.AppSettings["MandrillApiKey"];
                var result = new Mandrill.MandrillApi(mandrillApiKey).SendMessage(request);
                isSent = true;
            }
            catch
            {
                throw;
                //isSent = false;
            }

            return isSent;
        }
        public bool Send(string sendFromEmail, string sendToEmail, string subject, string body, DateTime sendAtGivenTime, IEnumerable<Mandrill.Models.EmailAttachment> attachments)
        {
            bool isSent = false;
            try
            {
                List<Mandrill.Models.EmailAddress> emails = new List<Mandrill.Models.EmailAddress>();

                var request = new SendMessageRequest(
                   new Mandrill.Models.EmailMessage
                   {
                       FromEmail = "chalmer@zilverband.com",
                       Html = "<html><body>" + body + "</body></html>",
                       Subject = subject,
                       To = new Mandrill.Models.EmailAddress[] { 
                        new Mandrill.Models.EmailAddress(sendToEmail)
                       },
                       Attachments = attachments
                   });
                request.SendAt = sendAtGivenTime;
                bool success = true;
                var result = new Mandrill.MandrillApi("Zxyg3WIbVGl4JfuMP4xM8w").SendMessage(request);
                isSent = true;
            }
            catch
            {
                isSent = false;
            }

            return isSent;
        }

    }
}
