﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;

namespace Cron.Notification
{
    public class Text
    {
        public string From { get; set; }
        public List<string> To { get; set; }
        public string Message { get; set; }

        public void Send()
        {
            try
            {

                // Find your Account Sid and Auth Token at twilio.com/user/account
                string AccountSid = ConfigurationManager.AppSettings["SID"];
                string AuthToken = ConfigurationManager.AppSettings["AuthToken"];

                var twilio = new TwilioRestClient(AccountSid, AuthToken);
                foreach (string to in To)
                {
                    var message = twilio.SendMessage(From, to, Message);

                    if (message.RestException != null)
                    {
                        string error = message.RestException.Message;
                        throw new Exception(error);
                    }
                }

                //Console.WriteLine(message.Sid);
            }
            catch
            {
                throw;
            }
  
        }
    }
}
