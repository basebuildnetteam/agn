﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron
{
    /*
    public class Reservation
    {
        public int Id { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Balance { get; set; }

        public List<Reservation> GetReservations()
        {
            return new List<Reservation>()
            {                
                new Reservation(){Id = 1, DueDate = DateTime.Today, Balance = 100},
                new Reservation(){Id = 2, DueDate = DateTime.Today.AddDays(3), Balance = 0},
                new Reservation(){Id = 3, DueDate = DateTime.Today.AddDays(4), Balance = 300},
                new Reservation(){Id = 4, DueDate = DateTime.Today.AddDays(3), Balance = 350},
                new Reservation(){Id = 5, DueDate = DateTime.Today, Balance = 500},
                new Reservation(){Id = 6, DueDate = DateTime.Today.AddDays(3), Balance = 550}
            };
        }
    }
    */
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Reservation_Id { get; set; }
        public string Email { get; set; }

        public List<Customer> GetCustomers()
        {
            return new List<Customer>()
            {
                new Customer(){Id = 1, Reservation_Id = 3, Email="customer1@gmail.com"},
                new Customer(){Id = 2, Reservation_Id = 1, Email="customer2@gmail.com"},
                new Customer(){Id = 3, Reservation_Id = 5, Email="customer3@gmail.com"},
                new Customer(){Id = 4, Reservation_Id = 4, Email="peterfb2014@gmail.com"},
                new Customer(){Id = 5, Reservation_Id = 6, Email="customer5@gmail.com"},
                new Customer(){Id = 6, Reservation_Id = 2, Email="customer6@gmail.com"}
            };
        }
    }
}
