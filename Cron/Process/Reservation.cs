﻿using Entity;
using RNSWebservice.WSPortal;
using RNSWebservice.WSReservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Cron.Models;
using Cron.Notification;
using System.Data.Entity;
using System.Configuration;
using System.Text.RegularExpressions;
using Cron.Utility;

namespace Cron.Process
{
    public class Reservation
    {
        AGNEntities _db = new AGNEntities();
        public enum Balance
        {
            Either = 1,
            Yes = 2,
            No = 3
        }
        private Settings _settings;
        public Reservation()
        {
            _settings = new Settings();
        }

        private void GetAndUpdateReservations()
        {
            ResvInfo res = new ResvInfo();
            GTHYapi wsGTHYapi = new GTHYapi();
            RNSPortalWS2 wsRNSPortalWS1 = new RNSPortalWS2();
            var properties = wsGTHYapi.GetAllProperties(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey);
            List<ResvInfo> rnsReservationsNew = new List<ResvInfo>();
            List<ResvInfo> rnsAllReservations = new List<ResvInfo>();
            
            List<ResvInfo> allReservations = new List<ResvInfo>();
            
            ////Adding ReservationGroups
            var rnsReservationGroups = wsRNSPortalWS1.GetReservationGroups(_settings.RNSPortalWS1Accesskey, "1").ToList();
            var bookings = wsRNSPortalWS1.GetBookingsAndCancellations(_settings.RNSPortalWS1Accesskey, "1", Common.UTCToTimezone(DateTime.UtcNow).Date.AddDays(-1).ToShortDateString());
            Cron.Models.ReservationGroup.Save(rnsReservationGroups);

            Cron.Process.UnitProperty prop = new Cron.Process.UnitProperty();
            prop.Save(properties.ToList());

            foreach (var property in properties)
            {               
                try
                {
                    rnsReservationsNew = new List<ResvInfo>();
                    rnsReservationsNew = wsGTHYapi.GetAllReservationsNew(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey, property.UnitNumber.ToString()).ToList();

                    //rnsAllReservations = new List<ResvInfo>();
                    //rnsAllReservations = wsGTHYapi.GetAllReservations(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey, property.Id.ToString()).ToList();
                    //allReservations.AddRange(rnsAllReservations);

                    Save(rnsReservationsNew);  //commented for test purposes and will be restored

                }
                catch
                {
                    continue;
                }
            }
        }
        public void Execute(string stopAlerMessage)
        {
            #region for Query Testing
            /*
            string query2 = @"  select R.GuestEmail
  from Reservation R
  inner join BookingAgent BA
    on R.RNSBookingAgent_Id = BA.RNSBookingAgent_Id
  inner join EmailTemplate ET
    on BA.RNSBookingAgent_Id = ET.RNSBookingAgent_Id
  inner join RefReservationType RRT
    on ET.RefReservationType_Id = RRT.Id
  inner join ReservationGroup GRP
    on R.RNSLocation_Id = GRP.RNSLocationId
  where cast('10/30/2015 12:00:00 AM' as datetime) --Current Date
   < -- < = Before or After
   DateAdd(day,-0,R.BookDate) --  2 = Send Days, BookDate = Event
  and BA.Id = 2  -- = AgentId
  and R.RNSReservation_Id = 745 -- = RNSReservation_Id
  and R.InternalType = RRT.Description --'GC' --EmailTemplae RefReservationType.Description
  and GRP.Id = ET.ReservationGroup_Id--@@ReservationGroup
--and BalanceDue = @@BalanceDue";
var myEmail = _db.Database.SqlQuery<string>(query2).FirstOrDefault();
*/
#endregion

            GetAndUpdateReservations();
            DateTime timezondDateNow = Common.UTCToTimezone(DateTime.UtcNow).Date; //.AddDays(-20);
            //List<Entity.Reservation> agnReservations = _db.Reservations.Where(x => (x.OptOut == false && x.Departure > utcDateNow)).ToList();
            //List<Entity.Reservation> agnReservations = _db.Reservations.Where(x => (x.OptOut == false)).ToList();
            List<Entity.Reservation> agnReservations = _db.Reservations.ToList();

            EmailNotification emaitNotification = new EmailNotification();
            emaitNotification.Notify(agnReservations, stopAlerMessage);
            TextNotification textNotifcation = new TextNotification();
            textNotifcation.Notify(agnReservations, stopAlerMessage);
        }
        public void Execute(SendEmailModel sendEmailModel, string currentUserEmail, string stopAlerMessage)
        {

           
            try
            {
                string currentAgentId = _db.BookingAgents.Where(x => x.Email == currentUserEmail).Select(x => x.RNSBookingAgent_Id).FirstOrDefault();
                DateTime timezoneDate;
                List<Entity.Reservation> agnReservations = new List<Entity.Reservation>();

                switch (sendEmailModel.sendTo)
                {
                    case 1: //Reservation
                        {
                            SendByReservation(sendEmailModel, stopAlerMessage);
                            break;
                        }
                    case 2: //Arrivals Today
                        {
                            GetAndUpdateReservations();
                            timezoneDate = Common.UTCToTimezone(DateTime.UtcNow).Date;
                            //restore this
                            //agnReservations = _db.Reservations.Where(x => (x.OptOut == false && x.Arrival > utcDate)).ToList();
                            agnReservations = _db.Reservations.Where(x => (x.Arrival > timezoneDate)).ToList();

                            if (sendEmailModel.emailtemplateid != null && agnReservations.Count > 0)
                            {
                                agnReservations = agnReservations.Where(x => x.RNSBookingAgent_Id == currentAgentId).ToList();
                            }
                            if (agnReservations.Count > 0)
                            {
                                SendByParameter(sendEmailModel, agnReservations, stopAlerMessage);
                            }
                            else
                            {
                                throw new Exception("There are no reservations found");
                            }
                            break;
                        }
                    case 3://Arrivals Tomorrow
                        {
                            GetAndUpdateReservations();
                            timezoneDate = Common.UTCToTimezone(DateTime.UtcNow).Date.AddDays(1);
                            //restore this
                            //agnReservations = _db.Reservations.Where(x => (x.OptOut == false && x.Arrival > utcDate)).ToList();
                            agnReservations = _db.Reservations.Where(x => (x.Arrival > timezoneDate)).ToList();

                            if (sendEmailModel.emailtemplateid != null && agnReservations.Count > 0)
                            {
                                agnReservations = agnReservations.Where(x => x.RNSBookingAgent_Id == currentAgentId).ToList();
                            }
                            if (agnReservations.Count > 0)
                            {
                                SendByParameter(sendEmailModel, agnReservations, stopAlerMessage);
                            }
                            else
                            {
                                throw new Exception("There are no reservations found");
                            }
                            break;
                        }
                    case 4: //Departures Today
                        {
                            GetAndUpdateReservations();
                            timezoneDate = Common.UTCToTimezone(DateTime.UtcNow).Date;
                            //restore this
                            //agnReservations = _db.Reservations.Where(x => (x.OptOut == false && x.Departure > utcDate)).ToList();
                            agnReservations = _db.Reservations.Where(x => (x.Departure > timezoneDate)).ToList();

                            if (sendEmailModel.emailtemplateid != null && agnReservations.Count > 0)
                            {
                                agnReservations = agnReservations.Where(x => x.RNSBookingAgent_Id == currentAgentId).ToList();
                            }
                            if (agnReservations.Count > 0)
                            {
                                SendByParameter(sendEmailModel, agnReservations, stopAlerMessage);
                            }
                            else
                            {
                                throw new Exception("There are no reservations found");
                            }
                            break;
                        }
                    case 5: //Departures Tomorrow
                        {
                            GetAndUpdateReservations();
                            timezoneDate = Common.UTCToTimezone(DateTime.UtcNow).Date.AddDays(1);
                            //restore this
                            //agnReservations = _db.Reservations.Where(x => (x.OptOut == false && x.Departure > utcDate)).ToList();
                            agnReservations = _db.Reservations.Where(x => (x.Departure > timezoneDate)).ToList();

                            if (sendEmailModel.emailtemplateid != null && agnReservations.Count > 0)
                            {
                                agnReservations = agnReservations.Where(x => x.RNSBookingAgent_Id == currentAgentId).ToList();
                            }
                            if (agnReservations.Count > 0)
                            {
                                SendByParameter(sendEmailModel, agnReservations, stopAlerMessage);
                            }
                            else
                            {
                                throw new Exception("There are no reservations found");
                            }
                            break;
                        }
                    case 6: //All In-House
                        {
                            GetAndUpdateReservations();
                            timezoneDate = Common.UTCToTimezone(DateTime.UtcNow).Date;
                            //restore this
                            //agnReservations = _db.Reservations.Where(x => (x.OptOut == false && x.Arrival <= utcDate && x.Departure >= utcDate)).ToList();
                            agnReservations = _db.Reservations.Where(x => (x.Arrival <= timezoneDate && x.Departure >= timezoneDate)).ToList();

                            if (sendEmailModel.emailtemplateid != null && agnReservations.Count > 0)
                            {
                                agnReservations = agnReservations.Where(x => x.RNSBookingAgent_Id == currentAgentId).ToList();
                            }
                            if (agnReservations.Count > 0)
                            {
                                SendByParameter(sendEmailModel, agnReservations, stopAlerMessage);
                            }
                            else
                            {
                                throw new Exception("There are no reservations found");
                            }
                            break;
                        }
                    default:
                        {
                            throw new Exception("The Send To filter is not recognized");
                            break;
                        }

                }
            }
            catch(Exception e)
            {
                throw;
            }
        
        }
        private void SendByReservation_2(SendEmailModel sendEmailModel, string stopAlerMessage)
        {
            List<int> listResNums = new List<int>();
            try
            {
                string[] rsNums = sendEmailModel.reservationnumbers.Split(',');
                foreach (string x in rsNums)
                {
                    listResNums.Add(int.Parse(x.Trim()));
                }
            }
            catch (Exception e)
            {
                //response.message = e.Message;
                //response.success = false;
                //return Json(response, JsonRequestBehavior.AllowGet);
            }
            try
            {

                GTHYapi wsGTHYapi = new GTHYapi();
                RNSPortalWS2 wsRNSPortalWS1 = new RNSPortalWS2();
                List<ResvInfo> rnsReservationsNew = new List<ResvInfo>();
                List<ResvInfo> rnsAllReservations = new List<ResvInfo>();
                List<ResvInfo> allReservations = new List<ResvInfo>();
                List<BookingsAndCancellationInfo> bookings = new List<BookingsAndCancellationInfo>();

                var properties = wsGTHYapi.GetAllProperties(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey);
                try
                {
                    bookings = wsRNSPortalWS1.GetBookingsAndCancellations(_settings.RNSPortalWS1Accesskey, "1", Common.UTCToTimezone(DateTime.UtcNow).Date.AddDays(-1).ToShortDateString()).ToList();
                }
                catch
                {
                    throw new Exception("No bookings found!");
                }
                ////For Testing
                //bookings.Add(new BookingsAndCancellationInfo
                //{
                //    AgentId = 12,
                //    ArriveDate = "12/4/2015",
                //    CompanyId = "1",
                //    DepartDate = "12/7/2015",
                //    Email = "chabstester@gmail.com",
                //    ReservationNumber = "720",
                //    ReservationType = "OO",
                //    TransId = 1,
                //    TransType = "OO",
                //    UnitId = 2
                //});


                Cron.Process.UnitProperty prop = new Cron.Process.UnitProperty();
                prop.Save(properties.ToList());

                List<BookingsAndCancellationInfo> bookingsFoundInReservations = new List<BookingsAndCancellationInfo>();
                foreach (var property in properties)
                {

                    if (listResNums.Count == 0) break;
                    try
                    {

                        rnsReservationsNew = new List<ResvInfo>();
                        rnsReservationsNew = wsGTHYapi.GetAllReservationsNew(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey, property.UnitNumber.ToString()).ToList();

                        rnsAllReservations = new List<ResvInfo>();
                        rnsAllReservations = wsGTHYapi.GetAllReservations(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey, property.Id.ToString()).ToList();
                        allReservations.AddRange(rnsAllReservations);

                        bookingsFoundInReservations = 
                            (
                            from boks in bookings
                            where boks.UnitId == property.Id
                            select boks
                            ).ToList();

                                               
                        if (bookingsFoundInReservations.Count == 0) 
                        { 
                            continue; 
                        }
                        
                         List<ResvInfo> reservationsInListResNums = new List<ResvInfo>();
                        reservationsInListResNums =
                                (
                                    from res in allReservations
                                    join boks in bookingsFoundInReservations on
                                     new { 
                                            JoinProperty1 = res.Arrival.Date,
                                            JoinProperty2 = res.Departure.Date
                                        } 
                                    equals 
                                    new { 
                                            JoinProperty1 = DateTime.Parse(boks.ArriveDate).Date,
                                            JoinProperty2 = DateTime.Parse(boks.DepartDate).Date
                                        }
                                    select  res 
                                ).Distinct().ToList();
                        Dictionary<int, string> dict = new Dictionary<int, string>();
                        dict = (
                                    from res in allReservations
                                    join boks in bookingsFoundInReservations on
                                     new
                                     {
                                         JoinProperty1 = res.Arrival.Date,
                                         JoinProperty2 = res.Departure.Date
                                     }
                                    equals
                                    new
                                    {
                                        JoinProperty1 = DateTime.Parse(boks.ArriveDate).Date,
                                        JoinProperty2 = DateTime.Parse(boks.DepartDate).Date
                                    }
                                    select new { res.ReservationId, boks.ReservationNumber }
                                ).ToDictionary(x => x.ReservationId, x => x.ReservationNumber);
                        Cron.Process.Reservation cronRes = new Cron.Process.Reservation();
                        cronRes.Save(allReservations);


                        foreach (ResvInfo r in reservationsInListResNums)
                        {

                            listResNums.Remove(int.Parse(dict[r.ReservationId])); //Remove from list those email that are sent
                            var agnReservation = _db.Reservations.Where(x => x.RNSReservation_Id == r.ReservationId).FirstOrDefault();
                            List<Entity.Reservation> agnReservations = new List<Entity.Reservation>();
                            agnReservations.Add(agnReservation);

                            if (sendEmailModel.sendAsEmail == true)
                            {                              
                                EmailNotification emaitNotification = new EmailNotification();
                                emaitNotification.Notify(agnReservations, sendEmailModel, stopAlerMessage);
                            }

                            if (sendEmailModel.sendAsText == true)
                            {
                                TextNotification textNotifcation = new TextNotification();
                                textNotifcation.Notify(agnReservations, sendEmailModel, stopAlerMessage);
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }

                }
            }
            catch
            {
                throw;
            }

        }

        private void SendByReservation(SendEmailModel sendEmailModel, string stopAlerMessage)
        {
            List<int> listResNums = new List<int>();
            try
            {
                string[] rsNums = sendEmailModel.reservationnumbers.Split(',');
                foreach (string x in rsNums)
                {
                    listResNums.Add(int.Parse(Regex.Match(x.Trim(), @"\d+").Value));
                }
            }
            catch (Exception e)
            {
                //response.message = e.Message;
                //response.success = false;
                //return Json(response, JsonRequestBehavior.AllowGet);
            }
            try
            {

                GTHYapi wsGTHYapi = new GTHYapi();
                RNSPortalWS2 wsRNSPortalWS2 = new RNSPortalWS2();
                List<ResvInfo> rnsReservationsNew = new List<ResvInfo>();
                List<ResvInfo> rnsAllReservations = new List<ResvInfo>();
                List<ResvInfo> allReservations = new List<ResvInfo>();

                var properties = wsGTHYapi.GetAllProperties(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey);


                Cron.Process.UnitProperty prop = new Cron.Process.UnitProperty();
                prop.Save(properties.ToList());

                foreach (var property in properties)
                {

                    if (listResNums.Count == 0) break;
                    try
                    {
                        rnsReservationsNew = new List<ResvInfo>();
                        rnsReservationsNew = wsGTHYapi.GetAllReservationsNew(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey, property.UnitNumber.ToString()).ToList();
                      
                        //rnsAllReservations = new List<ResvInfo>();
                        //rnsAllReservations = wsGTHYapi.GetAllReservations(_settings.GTHYapiMngrId, _settings.GTHYapiAccesskey, property.Id.ToString()).ToList();
                        //rnsAllReservations.AddRange(rnsReservationsNew);
                        //allReservations = rnsAllReservations.Distinct().ToList();

                        allReservations = rnsReservationsNew;

                        List<ResvInfo> resvationsInListResNums =
                            (
                                from res in allReservations
                                where listResNums.Contains(int.Parse(Regex.Match(res.ConfirmationNumber, @"\d+").Value))
                                select res
                            ).Distinct().ToList();

                   
                        Cron.Process.Reservation cronRes = new Cron.Process.Reservation();
                        cronRes.Save(resvationsInListResNums);

                        
                        foreach (ResvInfo r in resvationsInListResNums)
                        {

                            listResNums.Remove(int.Parse(Regex.Match(r.ConfirmationNumber, @"\d+").Value)); //Remove from list those email that are sent
                            var agnReservation = _db.Reservations.Where(x => x.RNSReservation_Id == r.ReservationId).FirstOrDefault();
                            List<Entity.Reservation> agnReservations = new List<Entity.Reservation>();
                            agnReservations.Add(agnReservation);

                            #region commented
                            /*
                            StringBuilder body = new StringBuilder();
                            if (sendEmailModel.emailtemplateid == null || sendEmailModel.emailtemplateid == -1)
                            {
                                body.Append(sendEmailModel.body);
                            }
                            else
                            {
                                body.Append(emailTemplate.Body);
                            }
                            body.Replace("[Guest First Name]", r.GuestFirstName);
                            body.Replace("[Guest Last Name]", r.GuestLastName);
                            body.Replace("[Reservation #]", r.ConfirmationNumber);
                            body.Replace("[Unit Number]", r.UnitNumber);
                            body.Replace("[Unit Name]", property.Name);
                            body.Replace("Unit Addres]", property.Steet + ", " + property.City + " " + property.PostalCode);
                            //body.Replace("[Lock Code]", property.l); 
                            body.Replace("[Arrival Date]", r.Arrival.ToString());
                            body.Replace("[Departure Date]", r.Departure.ToString());
                            body.Replace("[Id]", agnReservation.Id.ToString());
                            body.Replace("[ReservationId]", agnReservation.RNSReservation_Id.ToString());
                            */
                            #endregion

                            if (sendEmailModel.sendAsEmail == true)
                            {                              
                                EmailNotification emaitNotification = new EmailNotification();
                                emaitNotification.Notify(agnReservations, sendEmailModel, stopAlerMessage);
                            }

                            if (sendEmailModel.sendAsText == true)
                            {
                                TextNotification textNotifcation = new TextNotification();
                                textNotifcation.Notify(agnReservations, sendEmailModel, stopAlerMessage);
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }

                }
            }
            catch
            {
                throw;
            }

        }
        
        public void SendByParameter(SendEmailModel sendEmailModel, List<Entity.Reservation> agnReservations, string stopAlerMessage)
        {
            string emailBody = sendEmailModel.body;
            //if(sendEmailModel.emailtemplateid != null)
            //{
            //    EmailTemplate emailTemplate = new EmailTemplate();
            //    emailTemplate = _db.EmailTemplates.Where(x => x.Id == sendEmailModel.emailtemplateid).FirstOrDefault();
            //    if(emailTemplate!= null)
            //    {
            //        sendEmailModel.body = emailTemplate.Body;
            //    }               
            //}
            if (sendEmailModel.sendAsEmail == true)
            {
                EmailNotification emaitNotification = new EmailNotification();
                emaitNotification.Notify(agnReservations, sendEmailModel, stopAlerMessage);
            }

            if (sendEmailModel.sendAsText == true)
            {
                TextNotification textNotifcation = new TextNotification();
                textNotifcation.Notify(agnReservations, sendEmailModel, stopAlerMessage);
            }
        }
        private void Template()
        {

        }
        
        public void Save(ResvInfo rnsReservation)
        {
            try
            {
                
                Entity.Reservation entReservation = new Entity.Reservation()
                {
                    GuestEmail = rnsReservation.GuestEmail,
                    GuestFirstName = rnsReservation.GuestFirstName,
                    GuestLastName = rnsReservation.GuestLastName,
                    GuestPhone = rnsReservation.GuestPhone,
                    InternalType = rnsReservation.InternalType,
                    OptOut = false,
                    PropertyId = rnsReservation.PropertyId,
                    Type = rnsReservation.Type,
                    UnitNumber = rnsReservation.UnitNumber,
                    RNSReservation_Id = rnsReservation.ReservationId,
                    RNSBookingAgent_Id = rnsReservation.AgentId,
                    RNSLocation_Id = rnsReservation.LocationId,
                    PaidInFull = rnsReservation.PaidInFull
                };
                if (_db.Reservations.Any(x => x.RNSReservation_Id == rnsReservation.ReservationId))
                {
                    _db.Entry(entReservation).State = EntityState.Modified;
                }
                else
                {
                    _db.Reservations.Add(entReservation);
                }
                _db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        public void Save(List<ResvInfo> rnsReservations)
        {
            try
            {
                foreach (ResvInfo rnsReservation in rnsReservations)
                {

                   
                    Entity.Reservation entReservation = new Entity.Reservation()
                    {
                        GuestEmail = rnsReservation.GuestEmail,
                        GuestFirstName = rnsReservation.GuestFirstName,
                        GuestLastName = rnsReservation.GuestLastName,
                        GuestPhone = rnsReservation.GuestPhone,
                        InternalType = rnsReservation.InternalType,
                        OptOut = false,
                        PropertyId = rnsReservation.PropertyId,
                        Type = rnsReservation.Type,
                        UnitNumber = rnsReservation.UnitNumber,
                        RNSReservation_Id = rnsReservation.ReservationId,
                        RNSBookingAgent_Id = rnsReservation.AgentId,
                        RNSLocation_Id = rnsReservation.LocationId,
                        PaidInFull = rnsReservation.PaidInFull,
                        Arrival = rnsReservation.Arrival,
                        BookDate = rnsReservation.BookDate,
                        Departure = rnsReservation.Departure,
                        ConfirmationNumber = rnsReservation.ConfirmationNumber
                    };
                    if (_db.Reservations.Any(x => x.RNSReservation_Id == rnsReservation.ReservationId))
                    {
                        
                        entReservation.Id = _db.Reservations.Where(x => x.RNSReservation_Id == rnsReservation.ReservationId).Select(x => x.Id).FirstOrDefault();
                        _db.Entry(entReservation).State = EntityState.Modified;
                    }
                    else
                    {
                        _db.Reservations.Add(entReservation);
                    }
                }
                _db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        
    }
}
