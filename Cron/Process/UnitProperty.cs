﻿using Entity;
using RNSWebservice.WSPortal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cron.Process
{
    public class UnitProperty
    {
        
        AGNEntities _db = new AGNEntities();
        public void Save(List<RNSWebservice.WSReservation.UnitInfo> unitInfos)
        {
            try
            {
                foreach (var unit in unitInfos)
                {
                    try
                    {
                        
                        Entity.UnitInfo unitProperty = new Entity.UnitInfo()
                        {
                            Area = unit.Area,
                            Bathrooms = unit.Bathrooms,
                            Bedrooms = unit.Bedrooms,
                            BookingURL = unit.BookingURL,
                            City = unit.City,
                            Description = unit.Description,
                            Id = unit.Id,
                            IsActive = unit.IsActive,
                            Latitude = unit.Latitude,
                            Longitude = unit.Longitude,
                            Name = unit.Name,
                            PostalCode = unit.PostalCode,
                            PropertyNotes = unit.PropertyNotes,
                            PropertyURL = unit.PropertyURL,
                            Sleeps = unit.Sleeps,
                            State = unit.State,
                            Street = unit.Steet,
                            Type = unit.Type,
                            UnitNumber = unit.UnitNumber

                        };
                        if (_db.UnitInfoes.Any(x => x.Id == unit.Id))
                        {
                            unitProperty.Id = _db.UnitInfoes.Where(x => x.Id == unit.Id).Select(x => x.Id).FirstOrDefault();
                            _db.Entry(unitProperty).State = EntityState.Modified;
                        }
                        else
                        {
                            _db.UnitInfoes.Add(unitProperty);
                        }
                    }
                    catch
                    {
                        continue;
                    }

                }
                _db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
        
        
    }
    
}
