﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace AGN.CronProcess
{
   
    public class CleanAndReady : ITask, IRegisteredObject
    {
        private readonly object _lock = new object();

        private bool _shuttingDown;

        public CleanAndReady()
        {
            // Register this task with the hosting environment.
            // Allows for a more graceful stop of the task, in the case of IIS shutting down.
            HostingEnvironment.RegisterObject(this);
        }

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                    return;

                // Do work
                Cron.Process.Reservation process = new Cron.Process.Reservation();


                //string domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                string domain = ConfigurationManager.AppSettings["Domain"];
                string stopAlertlink = domain + "/api/AlertMessage/Stop?id=[Id]&reservationId=[ReservationId]";
                string stopAlerMessage = "To Stop just click this link " + stopAlertlink;

                process.Execute(stopAlerMessage);
            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }

            HostingEnvironment.UnregisterObject(this);
        }
    }
}