﻿using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AGN.CronProcess
{
    public class CronRegistry :Registry
    {
        public CronRegistry()
        {
            try
            {
                int runCleanAndReady = int.Parse(ConfigurationManager.AppSettings["CleanAndReadyFilter_RunEveryXMinutes"]);
                int runOtherFilters1 = int.Parse(ConfigurationManager.AppSettings["OtherFilters_RunEveryAtSpecificHour1"]);
                int runOtherFilters2 = int.Parse(ConfigurationManager.AppSettings["OtherFilters_RunEveryAtSpecificHour2"]);
                string executeCron = ConfigurationManager.AppSettings["ExecuteCron"];
                if (String.IsNullOrEmpty(executeCron) == false && executeCron.Trim().ToLower() == "true")
                {
                    Schedule<CleanAndReady>().ToRunNow().AndEvery(runCleanAndReady).Minutes();
                    Schedule<Reservation>().ToRunEvery(1).Days().At(runOtherFilters1, 0);
                    Schedule<Reservation>().ToRunEvery(1).Days().At(runOtherFilters2, 0);
                }
            }
            catch
            {
                throw new Exception("Invalid cron settings input in web.config");
            }
        }
    }
}