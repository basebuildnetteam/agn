﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entity;

namespace AGN.Controllers
{
    public class EmailTemplatesTestController : Controller
    {
        private AGNEntities db = new AGNEntities();

        // GET: EmailTemplatesTest
        public ActionResult Index()
        {
            //var emailTemplates = db.EmailTemplates.Include(e => e.RNSBookingAgent_Id).Include(e => e.RefEvent).Include(e => e.ReservationGroup).Include(e => e.RefReservationType).Include(e => e.RefWhen);
            //return View(emailTemplates.ToList());
        }

        // GET: EmailTemplatesTest/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = db.EmailTemplates.Find(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            return View(emailTemplate);
        }

        // GET: EmailTemplatesTest/Create
        public ActionResult Create()
        {
            ViewBag.BookingAgent_Id = new SelectList(db.BookingAgents, "Id", "Name");
            ViewBag.RefEvent_Id = new SelectList(db.RefEvents, "Id", "Description");
            ViewBag.ReservationGroup_Id = new SelectList(db.ReservationGroups, "Id", "Name");
            ViewBag.RefReservationType_Id = new SelectList(db.RefReservationTypes, "Id", "Description");
            ViewBag.RefWhen_Id = new SelectList(db.RefWhens, "Id", "Description");
            return View();
        }

        // POST: EmailTemplatesTest/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,SendDays,RefWhen_Id,RefEvent_Id,RefReservationType_Id,ReservationGroup_Id,BookingAgent_Id,FromEmail,BCC,Subject,Body")] EmailTemplate emailTemplate)
        {
            if (ModelState.IsValid)
            {
                db.EmailTemplates.Add(emailTemplate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BookingAgent_Id = new SelectList(db.BookingAgents, "RNSBookingAgent_Id", "Name", emailTemplate.RNSBookingAgent_Id);
            ViewBag.RefEvent_Id = new SelectList(db.RefEvents, "Id", "Description", emailTemplate.RefEvent_Id);
            ViewBag.ReservationGroup_Id = new SelectList(db.ReservationGroups, "Id", "Name", emailTemplate.ReservationGroup_Id);
            ViewBag.RefReservationType_Id = new SelectList(db.RefReservationTypes, "Id", "Description", emailTemplate.RefReservationType_Id);
            ViewBag.RefWhen_Id = new SelectList(db.RefWhens, "Id", "Description", emailTemplate.RefWhen_Id);
            return View(emailTemplate);
        }

        // GET: EmailTemplatesTest/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = db.EmailTemplates.Find(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookingAgent_Id = new SelectList(db.BookingAgents, "RNSBookingAgent_Id", "Name", emailTemplate.RNSBookingAgent_Id);
            ViewBag.RefEvent_Id = new SelectList(db.RefEvents, "Id", "Description", emailTemplate.RefEvent_Id);
            ViewBag.ReservationGroup_Id = new SelectList(db.ReservationGroups, "Id", "Name", emailTemplate.ReservationGroup_Id);
            ViewBag.RefReservationType_Id = new SelectList(db.RefReservationTypes, "Id", "Description", emailTemplate.RefReservationType_Id);
            ViewBag.RefWhen_Id = new SelectList(db.RefWhens, "Id", "Description", emailTemplate.RefWhen_Id);
            return View(emailTemplate);
        }

        // POST: EmailTemplatesTest/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,SendDays,RefWhen_Id,RefEvent_Id,RefReservationType_Id,ReservationGroup_Id,BookingAgent_Id,FromEmail,BCC,Subject,Body")] EmailTemplate emailTemplate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emailTemplate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookingAgent_Id = new SelectList(db.BookingAgents, "Id", "Name", emailTemplate.RNSBookingAgent_Id);
            ViewBag.RefEvent_Id = new SelectList(db.RefEvents, "Id", "Description", emailTemplate.RefEvent_Id);
            ViewBag.ReservationGroup_Id = new SelectList(db.ReservationGroups, "Id", "Name", emailTemplate.ReservationGroup_Id);
            ViewBag.RefReservationType_Id = new SelectList(db.RefReservationTypes, "Id", "Description", emailTemplate.RefReservationType_Id);
            ViewBag.RefWhen_Id = new SelectList(db.RefWhens, "Id", "Description", emailTemplate.RefWhen_Id);
            return View(emailTemplate);
        }

        // GET: EmailTemplatesTest/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = db.EmailTemplates.Find(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            return View(emailTemplate);
        }

        // POST: EmailTemplatesTest/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailTemplate emailTemplate = db.EmailTemplates.Find(id);
            db.EmailTemplates.Remove(emailTemplate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
