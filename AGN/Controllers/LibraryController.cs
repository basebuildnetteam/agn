﻿using AGN.Models;
using Cron.Models;
using Entity;
using RNSWebservice.WSPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
    public class LibraryController : Controller
    {
        private AGNEntities _db = new AGNEntities();
        private RNSPortalWS2 wsRNSPortalWS2 = new RNSPortalWS2();
        private Settings _settings = new Settings();
        public JsonResult BookingAgents()
        {
            var agents = wsRNSPortalWS2.GetAgents(_settings.RNSPortalWS1Accesskey, _settings.RNSPortalWS1CoId.ToString()).ToList();
            string currentUserEmail = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string agentLocationId = _db.BookingAgents.Where(x => x.Email == currentUserEmail).Select(x => x.RNSLocationId).FirstOrDefault();
            int locationId;
            if (string.IsNullOrEmpty(agentLocationId))
            {
                locationId = 1;
            }
            else
            {
                locationId = Convert.ToInt32(agentLocationId);
            }
            var data = from x in agents
                       where x.RNSLocationId == locationId
                         select new
                         {
                             id = x.Id,
                             name = x.FirstName + " "+ x.LastName
                         };
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult BookingAgents(int id)
        {
            var agents = wsRNSPortalWS2.GetAgents(_settings.RNSPortalWS1Accesskey, _settings.RNSPortalWS1CoId.ToString()).ToList();
            string currentUserEmail = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string agentLocationId = _db.BookingAgents.Where(x => x.Email == currentUserEmail).Select(x => x.RNSLocationId).FirstOrDefault();
            int locationId;
            if (string.IsNullOrEmpty(agentLocationId))
            {
                locationId = 1;
            }
            else
            {
                locationId = Convert.ToInt32(agentLocationId);
            }
            var data = from x in agents
                       where x.Id == id
                       select new
                       {
                           id = x.Id,
                           name = x.FirstName + " " + x.LastName
                       };
            //var data = from x in _db.BookingAgents
            //            where x.Id == id
            //            select new
            //            {
            //                id = x.Id,
            //                rnsbookingagentid = x.RNSBookingAgent_Id,
            //                email = x.Email,
            //                cellnumber = x.CellNumber,
            //                name = x.Name
            //            };
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet); 
        }

        public JsonResult RefEvents()
        {
            var data = (from x in _db.RefEvents
                        select new
                        {
                            id = x.Id,
                            description = x.Description
                        }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RefEvents(int id)
        {
            var data = (from x in _db.RefEvents
                        where x.Id == id
                        select new
                        {
                            id = x.Id,
                            description = x.Description
                        }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data.ToList() };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RefReservationTypes()
        {
            string currentUserEmail =ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string agentLocationId = _db.BookingAgents.Where(x => x.Email == currentUserEmail).Select(x => x.RNSLocationId).FirstOrDefault();
            var reservationTypes = wsRNSPortalWS2.GetReservationTypes(_settings.RNSPortalWS1Accesskey, "1", int.Parse(agentLocationId)).ToList();

            var data = (from x in reservationTypes
                       select new
                       {
                           id = x.ResvCode,
                           name = x.ResvCode
                           //description = x.ResvTypeDescription
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult RefReservationTypes(int id)
        {
            string currentUserEmail = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string agentLocationId = _db.BookingAgents.Where(x => x.Email == currentUserEmail).Select(x => x.RNSLocationId).FirstOrDefault();
            var reservationTypes = wsRNSPortalWS2.GetReservationTypes(_settings.RNSPortalWS1Accesskey, "1", int.Parse(agentLocationId)).ToList();

            var data = (from x in reservationTypes
                       where x.ResvCode == id.ToString()
                       select new
                       {
                           id = x.ResvCode,
                           name = x.ResvCode
                           //description = x.ResvTypeDescription
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RefWhens()
        {
            var data = (from x in _db.RefWhens
                       select new
                       {
                           id = x.Id,
                           description = x.Description
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RefWhens(int id)
        {
            var data = (from x in _db.RefWhens
                       where x.Id == id
                       select new
                       {
                           id = x.Id,
                           description = x.Description
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ReservationGroups()
        {
            var reservationGroups = wsRNSPortalWS2.GetReservationGroups(_settings.RNSPortalWS1Accesskey, "1").ToList();
            string currentUserEmail = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string agentLocationId = _db.BookingAgents.Where(x => x.Email == currentUserEmail).Select(x => x.RNSLocationId).FirstOrDefault();
            int locationId;
            if(string.IsNullOrEmpty(agentLocationId))
            {
                locationId = 1;
            }
            else
            {
                locationId = Convert.ToInt32(agentLocationId);
            }

            var data = (from x in reservationGroups
                        where x.RNSLocationId == locationId
                       select new
                       {
                           id = x.Id,
                           name = x.Name
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ReservationGroups(int id)
        {
            var reservationGroups = wsRNSPortalWS2.GetReservationGroups(_settings.RNSPortalWS1Accesskey, "1").ToList();

            var data = (from x in reservationGroups
                       where x.Id == id
                       select new
                       {
                           id = x.Id,
                           name = x.Name
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        
        public JsonResult SendTos()
        {
            var data = (from x in _db.RefSendToes.OrderBy(x=>x.SeqNum)
                       select new
                       {
                           id = x.Id,
                           description = x.Description,
                           seq = x.SeqNum
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SendTo(int id)
        {
            var data = (from x in _db.RefSendToes
                       where x.Id == id
                       select new
                       {
                           id = x.Id,
                           description = x.Description,
                           seq = x.SeqNum
                       }).ToList();
            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmailAndTextTemplates()
        {
            var data = _db.EmailTemplates.Select(x => new{
                id = "email_" + x.Id.ToString(),
                name = "Email - " + x.Name
            }).ToList();
            var textTemplates = _db.TextTemplates.Select(x => new
            {
                id = "text_" + x.Id.ToString(),
                name = "Text - " + x.Name
            }).ToList();

            data.AddRange(textTemplates);

            ResponseModel response = new ResponseModel() { success = true, message = string.Empty, data = data };

            return Json(response, JsonRequestBehavior.AllowGet);
        }
         
    }
}