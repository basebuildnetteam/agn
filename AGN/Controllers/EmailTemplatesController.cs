﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net;

namespace AGN.Controllers
{
     
    public class EmailTemplatesController : Controller
    {
        private AGNEntities _db = new AGNEntities();

        // GET: EmailTemplates
        public ActionResult Index()
        {
            var emailTemplates = _db.EmailTemplates.Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            ViewBag.EmailTemplate_Id = new SelectList(emailTemplates, "Id", "Name");
            return View();
        }

        //GET
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailTemplate emailTemplate = _db.EmailTemplates.Find(id);
            if (emailTemplate == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookingAgent_Id = new SelectList(_db.BookingAgents, "Id", "Name", emailTemplate.RNSBookingAgent_Id);
            ViewBag.RefEvent_Id = new SelectList(_db.RefEvents, "Id", "Description", emailTemplate.RefEvent_Id);
            ViewBag.ReservationGroup_Id = new SelectList(_db.ReservationGroups, "Id", "Name", emailTemplate.ReservationGroup_Id);
            ViewBag.RefReservationType_Id = new SelectList(_db.RefReservationTypes, "Id", "Description", emailTemplate.RefReservationType_Id);
            ViewBag.RefWhen_Id = new SelectList(_db.RefWhens, "Id", "Description", emailTemplate.RefWhen_Id);
            return View(emailTemplate);
        }

        // POST: EmailTemplatesTest/Edit/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,SendDays,RefWhen_Id,RefEvent_Id,RefReservationType_Id,ReservationGroup_Id,BookingAgent_Id,FromEmail,BCC,Subject,Body")] EmailTemplate emailTemplate)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(emailTemplate).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BookingAgent_Id = new SelectList(_db.BookingAgents, "Id", "Name", emailTemplate.RNSBookingAgent_Id);
            ViewBag.RefEvent_Id = new SelectList(_db.RefEvents, "Id", "Description", emailTemplate.RefEvent_Id);
            ViewBag.ReservationGroup_Id = new SelectList(_db.ReservationGroups, "Id", "Name", emailTemplate.ReservationGroup_Id);
            ViewBag.RefReservationType_Id = new SelectList(_db.RefReservationTypes, "Id", "Description", emailTemplate.RefReservationType_Id);
            ViewBag.RefWhen_Id = new SelectList(_db.RefWhens, "Id", "Description", emailTemplate.RefWhen_Id);
            return View(emailTemplate);
        }

    }
}