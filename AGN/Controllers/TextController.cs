﻿using AGN.Models;
using Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
     
    [RoutePrefix("text")]
    public class TextController : Controller
    {
        private AGNEntities _db = new AGNEntities();

        [Route("templates")]
        public ActionResult Templates()
        {
            return View("~/Views/Text/Templates.cshtml");
        }

        [Route("templates/list")]
        public JsonResult List()
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var templates = _db.TextTemplates.Select(x => new
                {
                    id = x.Id,
                    name = x.Name
                }).ToList();

                response.success = true;
                response.message = string.Empty;
                response.data = templates;
            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.success = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Create()
        {
            return View();
        }

        [Route("templates/details/{id}")]
        public JsonResult Detail(int id)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var template = _db.TextTemplates.Where(x => x.Id == id).Select(tmp => new
                {
                    id = tmp.Id,
                    name = tmp.Name,
                    days = tmp.SendDays,
                    whenid = tmp.RefWhen_Id,
                    eventid = tmp.RefEvent_Id,
                    reservationtype = tmp.RefReservationType_Id,
                    reservationgroup = tmp.ReservationGroup_Id,
                    bookingagent = tmp.RNSBookingAgent_Id,
                    from = tmp.MobileNumber,
                    body = tmp.Body,
                    balancedue = tmp.BalanceDue
                }).FirstOrDefault();

                response.success = true;
                response.data = template;
            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(TextTemplate textTemplate)
        {

            ResponseModel response = new ResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    string currentLoginAgentEmail = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
                    if (textTemplate.MobileNumber == null)
                    {
                        textTemplate.MobileNumber = _db.BookingAgents.Where(x => x.Email == currentLoginAgentEmail).Select(x => x.CellNumber).FirstOrDefault(); 
                    }
                    //string currentAgentId = _db.BookingAgents.Where(x => x.Email == currentLoginAgentEmail).Select(x => x.RNSBookingAgent_Id).FirstOrDefault();
                    //textTemplate.RNSBookingAgent_Id = String.IsNullOrWhiteSpace(currentAgentId) ? 0 : int.Parse(currentAgentId);


                    if(textTemplate.Id > 0){
                        _db.Entry(textTemplate).State = EntityState.Modified;
                    }
                    else
                    {
                        TextTemplate template = new TextTemplate()
                        {
                            BalanceDue = textTemplate.BalanceDue,
                            Body = textTemplate.Body,
                            Name = textTemplate.Name,
                            RefEvent_Id = textTemplate.RefEvent_Id,
                            RefReservationType_Id = textTemplate.RefReservationType_Id,
                            RefWhen_Id = textTemplate.RefWhen_Id,
                            ReservationGroup_Id = textTemplate.ReservationGroup_Id,
                            RNSBookingAgent_Id = textTemplate.RNSBookingAgent_Id,
                            SendDays = textTemplate.SendDays,
                            MobileNumber = textTemplate.MobileNumber
                             
                        };
                        _db.TextTemplates.Add(template);
                    }

                    _db.SaveChanges();


                    response.success = true;
                    response.message = "Success";
                    response.data = null;
                }
                else
                {
                    response.success = false;
                    response.message = "Success";
                    response.data = null;
                }
            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
                response.data = null;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [Route("templates/edit/{id}")]
        public ActionResult EditTemplate(int id)
        {
            return View("Edit");
        }

        [Route("templates/edit/")]
        public ActionResult EditTemplate()
        {
            return View("Edit");
        }

        [Route("templates/copy/{id}")]
        public JsonResult CopyTemplate(int id, string name)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var template = _db.TextTemplates.Find(id);
                var newTemplate = new TextTemplate()
                {
                    Name = name,
                    SendDays = template.SendDays,
                    RefWhen_Id = template.RefWhen_Id,
                    RefEvent_Id = template.RefEvent_Id,
                    RefReservationType_Id = template.RefReservationType_Id,
                    ReservationGroup_Id = template.ReservationGroup_Id,
                    RNSBookingAgent_Id = template.RNSBookingAgent_Id,
                    Body = template.Body,
                    BalanceDue = template.BalanceDue,
                    MobileNumber = template.MobileNumber,
                };

                newTemplate = _db.TextTemplates.Add(newTemplate);
                _db.SaveChanges();

                response.success = true;
                response.data = new
                {
                    id = newTemplate.Id,
                    name = newTemplate.Name
                };
            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var textTemplate = _db.TextTemplates.Find(id);
            if (textTemplate == null)
            {
                return HttpNotFound();
            }

            return View(textTemplate);
        }

        [HttpPost]
        public JsonResult Edit(TextTemplate textTemplate)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(textTemplate).State = EntityState.Modified;
                    _db.SaveChanges();

                    response.success = true;
                    response.message = "Success";
                    response.data = null;
                }
                else
                {
                    response.success = false;
                    response.message = "Please fill-in required fields";
                    response.data = textTemplate;
                }

            }
            catch
            {
                response.success = true;
                response.message = "Success";
                response.data = textTemplate;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}