﻿using AGN.Models;
using Cron;
using Entity;
using RNSWebservice.WSPortal;
using RNSWebservice.WSReservation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
     
    [RoutePrefix("email")]
    public class EmailController : Controller
    {
        private AGNEntities _db = new AGNEntities();

        [Route("templates")]
        public ActionResult Templates()
        {
            return View("~/Views/Email/Templates.cshtml");
        }

        [Route("templates/details/{id}")]
        public JsonResult Detail(int id)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var template = _db.EmailTemplates.Where(x => x.Id == id).Select(tmp => new
                {
                    id = tmp.Id,
                    name = tmp.Name,
                    days = tmp.SendDays,
                    whenid = tmp.RefWhen_Id,
                    eventid = tmp.RefEvent_Id,
                    reservationtype = tmp.RefReservationType_Id,
                    reservationgroup = tmp.ReservationGroup_Id,
                    bookingagent = tmp.RNSBookingAgent_Id,
                    from = tmp.FromEmail,
                    bcc = tmp.BCC,
                    subject = tmp.Subject,
                    body = tmp.Body,
                    balancedue = tmp.BalanceDue
                }).FirstOrDefault();

                response.success = true;
                response.data = template;
            }catch(Exception e){
                response.success = false;
                response.message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [Route("templates/list")]
        public JsonResult List()
        {
            ResponseModel response = new ResponseModel();
            try
            {                
                var templates = _db.EmailTemplates.Select(x => new
                {
                    id =  x.Id,
                    name = x.Name
                }).ToList();

                response.success = true;
                response.message = string.Empty;
                response.data = templates;
            }catch(Exception e){
                response.message = e.Message;
                response.success = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

         [Route("templates/edit/{id}")]
        public ActionResult EditTemplate(int id)
        {
            return View("Edit");
        }


         [Route("templates/edit/")]
         public ActionResult EditTemplate()
         {
             return View("Edit");
         }

        [Route("refevents/list")]
        public JsonResult RefList()
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var refevents = _db.RefEvents.Select(x => new
                {
                    id = x.Id,
                    name = x.Description
                }).ToList();

                response.success = true;
                response.message = string.Empty;
                response.data = refevents;
            }
            catch (Exception e)
            {
                response.message = e.Message;
                response.success = false;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [Route("templates/copy/{id}")]
        public JsonResult CopyTemplate(int id, string name)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var template =  _db.EmailTemplates.Find(id);
                var newTemplate = new EmailTemplate()
                {
                    Name = name,
                    SendDays = template.SendDays,
                    RefWhen_Id = template.RefWhen_Id,
                    RefEvent_Id =template.RefEvent_Id,
                    RefReservationType_Id = template.RefReservationType_Id,
                    ReservationGroup_Id = template.ReservationGroup_Id,
                    RNSBookingAgent_Id = template.RNSBookingAgent_Id,
                    FromEmail = template.FromEmail,
                    BCC = template.BCC,
                    Subject = template.Subject,
                    Body = template.Body,
                    BalanceDue = template.BalanceDue
                };

                newTemplate = _db.EmailTemplates.Add(newTemplate);
                _db.SaveChanges();

                response.success = true;
                response.data = new
                {
                    id = newTemplate.Id,
                    name = newTemplate.Name
                };
            }catch(Exception e){
                response.success = false;
                response.message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Send()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Send(SendEmailModel sendEmailModel)
        {

            ResponseModel response = new ResponseModel();
            try
            {
                response.success = false;
                response.message = "an error occurred while sending email";
                
                //HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                //doc.LoadHtml(sendEmailModel.body);
                //sendEmailModel.body = doc.DocumentNode.InnerText;

                string domain = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                string stopAlertlink = domain + "/api/AlertMessage/Stop?id=[Id]&reservationId=[ReservationId]";
                string stopAlerMessage = "To Stop just click this link " + stopAlertlink;
                  

                string currentUserEmail =ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (string.IsNullOrWhiteSpace(sendEmailModel.bcc) == true)
                {
                    sendEmailModel.bcc = currentUserEmail;
                }

                Cron.Process.Reservation reservation = new Cron.Process.Reservation();
                reservation.Execute(sendEmailModel, currentUserEmail, stopAlerMessage);
                //reservation.Execute(stopAlerMessage); 
                

                response.success = true;
                response.message = "Emails successfully sent";
            }
            catch(Exception e)
            {
                response.message = e.Message;
                response.success = false;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Create(EmailTemplate emailTemplate)
        {

            ResponseModel response = new ResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    string currentLoginAgentEmail =ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
                    if(emailTemplate.FromEmail== null)
                    {
                        emailTemplate.FromEmail = currentLoginAgentEmail;
                    }
                    //string currentAgentId = _db.BookingAgents.Where(x => x.Email == currentLoginAgentEmail).Select(x => x.RNSBookingAgent_Id).FirstOrDefault();
                    //emailTemplate.RNSBookingAgent_Id = String.IsNullOrWhiteSpace(currentAgentId) ? 0 : int.Parse(currentAgentId);

                    if(emailTemplate.Id > 0){
                        _db.Entry(emailTemplate).State = EntityState.Modified;
                    }
                    else
                    {
                        EmailTemplate template = new EmailTemplate()
                        {
                            BalanceDue = emailTemplate.BalanceDue,
                            BCC = emailTemplate.BCC,
                            Body = emailTemplate.Body,
                            FromEmail = emailTemplate.FromEmail,
                            Name = emailTemplate.Name,
                            RefEvent_Id = emailTemplate.RefEvent_Id,
                            RefReservationType_Id = emailTemplate.RefReservationType_Id,
                            RefWhen_Id = emailTemplate.RefWhen_Id,
                            ReservationGroup_Id = emailTemplate.ReservationGroup_Id,
                            RNSBookingAgent_Id = emailTemplate.RNSBookingAgent_Id,
                            SendDays = emailTemplate.SendDays,
                            Subject = emailTemplate.Subject
                        };
                        _db.EmailTemplates.Add(template);
                    }
                    
                    _db.SaveChanges();


                    response.success = true;
                    response.message = "Success";
                    response.data = null;
                }
                else
                {
                    response.success = false;
                    response.message = "Please fill in required fields.";
                    response.data = null;
                }
            }
            catch (Exception e)
            {
                response.success = false;
                response.message = e.Message;
                response.data = null;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var emailTemplates = _db.EmailTemplates.Find(id);
            if (emailTemplates == null)
            {
                return HttpNotFound();
            }
            
            return View(emailTemplates);
        }

        [HttpPost]
        public JsonResult Edit(EmailTemplate emailTemplate)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(emailTemplate).State = EntityState.Modified;
                    _db.SaveChanges();

                    response.success = true;
                    response.message = "Success";
                    response.data = null;
                }
                else
                {
                    response.success = false;
                    response.message = "Please fill-in required fields";
                    response.data = emailTemplate;
                }

            }
            catch
            {
                response.success = true;
                response.message = "Success";
                response.data = emailTemplate;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}