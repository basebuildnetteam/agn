﻿using AGN.Models;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
     
    [RoutePrefix("reports")]
    public class ReportsController : Controller
    {
        private AGNEntities _db = new AGNEntities();
        public ActionResult Index()
        {
            return View();
        }


        [Route("email")]
        public ActionResult Email()
        {
            ViewBag.Section = "Email";
            return View("RangePicker");
        }

        [Route("text")]
        public ActionResult Text()
        {
            ViewBag.Section = "Text";
            return View("RangePicker");
        }

        [Route("details/{section}")]
        public ActionResult Details(string section, string from, string to)
        {
            ViewBag.Section = section == "email" ? "Email" : "Text";
            return View("Details");
        }

        [Route("{section}/list")]
        [HttpGet]
        public JsonResult List(string section, string from, string to)
        {
            ResponseModel response = new ResponseModel();
            //weird stuff, from value goes to to, and to goes to from,
            //so im just swapping them for now, until i figure out this weird stuff
            DateTime fromDate = DateTime.Parse(to).Date;
            DateTime toDate = DateTime.Parse(from).Date;
            string formatDate = "MM/dd/yyyy";
            if(section == "email")
            {

                //var list = _db.EmailSents.OrderBy(x => x.DatetimeSent)
                //    .Where(x => x.DatetimeSent >= fromDate && x.DatetimeSent <= toDate)
                //    .Select(x => new
                //    {
                //        date_sent = x.DatetimeSent.ToString(),
                //        type_id = x.EmailTemplate_Id,
                //        type = x.EmailTemplate.Name,
                //        destination = x.SentTo
                //    }).ToList();

                var list =    (  from sents in _db.EmailSents
                                 .Where(s => s.DatetimeSent >= fromDate && s.DatetimeSent <= toDate)
                                 from templates in _db.EmailTemplates
                                      .Where(t => sents.EmailTemplate_Id == t.Id)
                                      .DefaultIfEmpty()
                                 select new
                                 {
                                     date_sent = sents.DatetimeSent.ToString(),
                                     type_id = sents.EmailTemplate_Id,
                                     type = templates.Name==null?"":templates.Name,
                                     destination = sents.SentTo
                                 }).OrderBy(x => x.date_sent).ToList();
               //for(int i=0; i<list.Count; i++)
               //{
               //    list[i].type = list[i].type == null ? "" : list[i].type;
               //}
                response.data = list;
            }
            else
            {               

                //var list = _db.TextSents.OrderBy(x => x.DatetimeSent)
                //    .Where(x => x.DatetimeSent >= fromDate && x.DatetimeSent <= toDate)
                //    .Select(x => new
                //    {
                //        date_sent = x.DatetimeSent.ToString(),
                //        type_id = x.TextTemplate_Id,
                //        type = x.TextTemplate.Name,
                //        destination = x.SentTo
                //    }).ToList();

                //response.data = list;

                var list = (from sents in _db.TextSents
                            .Where(s => s.DatetimeSent >= fromDate && s.DatetimeSent <= toDate)
                            from templates in _db.TextTemplates
                                 .Where(t => sents.TextTemplate_Id == t.Id)
                                 .DefaultIfEmpty()
                            select new
                            {
                                date_sent = sents.DatetimeSent.ToString(),
                                type_id = sents.TextTemplate_Id,
                                type = templates.Name,
                                destination = sents.SentTo
                            }).OrderBy(x => x.date_sent).ToList();
                response.data = list;
            }


            /*
            //list format (please maintain the variable names)
            response.data = new object[]{
                new{
                    date_sent = "1/15/2015",
                    type_id = 1,
                    type = "Welcome Reservation",
                    destination = "@mcsmith" //email or cellnumber
                },
                new{
                    date_sent = "1/16/2015",
                    type_id = 2,
                    type = "Payment Due",
                    destination = "@mcsmith" //email or cellnumber
                },
                                new{
                    date_sent = "1/17/2015",
                    type_id = 2,
                    type = "Payment Due",
                    destination = "@mcsmith" //email or cellnumber
                }
            };
            */
            response.success = true;

            return Json(response,JsonRequestBehavior.AllowGet);
        }
    }
}