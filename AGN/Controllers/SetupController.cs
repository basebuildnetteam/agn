﻿using AGN.Models;
using Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
     
    public class SetupController : Controller
    {
        AGNEntities _db = new AGNEntities();
        // GET: Setup
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var setups = _db.Setups.Find(id);
            if (setups == null)
            {
                return HttpNotFound();
            }

            return View(setups);
        }

        [HttpPost]
        public JsonResult Edit(Setup setup)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    _db.Entry(setup).State = EntityState.Modified;
                    _db.SaveChanges();

                    response.success = true;
                    response.message = "Success";
                    response.data = null;
                }
                else
                {
                    response.success = false;
                    response.message = "Please fill-in required fields";
                    response.data = setup;
                }

            }
            catch
            {
                response.success = true;
                response.message = "Success";
                response.data = setup;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Details()
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var setup = _db.Setups.Select(s => new
                {
                    id = s.Id,
                    smtpServer = s.SMTPServer,
                    email = s.SMTPEmail,
                    port = s.Port,
                    ssl = true,
                    password = s.Password,
                    defaultEmail = s.DefaultEmailAddress,
                    defaultMobile = s.DefaultMobileNumber
                }).FirstOrDefault();

                response.success = true;
                response.data = setup;
            }catch(Exception e){
                response.success = false;
                response.message = e.Message;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}