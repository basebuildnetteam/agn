﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
         
        public ActionResult Index()
        {
            ViewBag.Title = "Home";
            var claimsIdentity = User.Identity as ClaimsIdentity;
           
            return View();
        }
    }
}
