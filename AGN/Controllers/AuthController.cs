﻿using AGN.Models;
using Entity;
using RNSWebservice.WSPortal;
//using Rns.APIManager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace AGN.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult LogIn(string returnUrl)
        {
            if (returnUrl == "/") returnUrl = "/Dashboard/Index";

            //ViewBag.ReturnUrl = returnUrl;
            var model = new LoginModel
            {
                ReturnUrl = returnUrl
            };
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogIn(LoginModel model)//, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            string returnUrl = model.ReturnUrl == "/" ? "/Dashboard/Index" : model.ReturnUrl;

            RNSPortalWS2 wsRNSPortalWS2 = new RNSPortalWS2();
            var loginResult = wsRNSPortalWS2.AgentLogin(ConfigurationManager.AppSettings["RNSPortalWS1Accesskey"], model.Email, model.Password);
            bool accessGranted = false;
            if(loginResult.Id != 0 && loginResult.LoginStatus != false)
            {
                accessGranted = true;
            }

            bool isTestMode = ConfigurationManager.AppSettings["IsLiveMode"] == "0" ? true : false;
            if (accessGranted == true || (model.Email == "test@admin.com" && model.Password == "test" && isTestMode))
            {
                if (model.Email == "test@admin.com")
                {
                    loginResult = new AgentInfo()
                    {
                        FirstName = "test",
                        Id = 12,
                        LastName = "tester"
                    };
                }
                if (loginResult != null && !(model.Email == "test@admin.com" && model.Password == "test"))
                {
                    AGNEntities db = new AGNEntities();
                    var agentId = (from x in db.BookingAgents
                                              where x.RNSBookingAgent_Id == loginResult.Id.ToString()
                                              select x.Id
                                             ).FirstOrDefault();
                    
                        BookingAgent bookingAgent = new BookingAgent();
                        bookingAgent.Name = loginResult.FirstName + " " + loginResult.LastName;
                        bookingAgent.Password = loginResult.FirstName + " " + model.Password;
                        bookingAgent.RNSBookingAgent_Id = loginResult.Id.ToString();
                        bookingAgent.Username = model.Email;
                        bookingAgent.Email = model.Email;
                        bookingAgent.RNSLocationId = loginResult.RNSLocationId == null? "1" : loginResult.RNSLocationId.ToString();
                        
                        if (agentId == null || agentId == 0)
                        {
                            db.BookingAgents.Add(bookingAgent); 
                        }
                        else
                        {
                            bookingAgent.Id = int.Parse(agentId.ToString());
                            db.Entry(bookingAgent).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                }

                //FormsAuthentication.SetAuthCookie(model.AgentId, false);
                var identity = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.NameIdentifier, model.Email)
                },
                    "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);
                return Redirect(GetRedirectUrl(returnUrl));
            }
            

            // user authN failed
            ModelState.AddModelError("", "Invalid email or password");
            return View();
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                //return Url.Action("Index", "Home");
                return Url.Action("Index", "Dashboard");
            }

            return returnUrl;
        }

        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("index", "home");
        }
    }
}