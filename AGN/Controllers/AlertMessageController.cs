﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AGN.Controllers
{
    public class AlertMessageController : ApiController
    {
        public async Task<IHttpActionResult> Stop(int id, int reservationId)
        {
            AGNEntities db = new AGNEntities();
            bool stopAlert = false;
            stopAlert = db.Reservations.Any(x => (x.Id == id && x.RNSReservation_Id == reservationId));
            if(stopAlert)
            {
                try
                {
                    Reservation res = db.Reservations.Where(x => (x.Id == id && x.RNSReservation_Id == reservationId)).FirstOrDefault();
                    res.OptOut = true;
                    db.Entry(res).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    return StatusCode(HttpStatusCode.OK);
                }
                catch
                {
                    return StatusCode(HttpStatusCode.NotAcceptable);
                }
            }
            else
            {
                return NotFound();
            }
        }
    }
}
