﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGN.Models
{
    public class ResponseModel
    {
        public string message {get; set;}
        public bool success { get; set; }
        public object data { get; set; }

    }
}