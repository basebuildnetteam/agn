﻿$(function () {

    $('#setup-container')
        .on('click', '#btnsave', function () {
            showMessage('Saving details, please wait...', MSG_TYPE.LOADING);
            ajaxclient.saveSetup({
                Id: $('#setup_id').val(),
                SMTPServer : $('#smtp-server').val(),
                SMTPEmail: $('#smtp-email').val(),
                Port: $('#smtp-port').val(),
                Password: $('#smtp-password').val(),
                DefaultEmailAddress: $('#default-email').val(),
                DefaultMobileNumber: $('#default-text').val()
            }).done(function (res) {
                if(res.success){
                    showMessage('Successfully updated Settings.', MSG_TYPE.SUCCESS);
                } else {
                    showMessage('Failed to update Settings.', MSG_TYPE.WARNING);
                }
            });
        })
        .on('click', '#back', function () {
            location.href = '/dashboard';
        })
    ;

    showMessage('Retrieving details, please wait...', MSG_TYPE.LOADING);
    ajaxclient.getSetupDetails().done(function (res) {
        showMessage('Successfully retrieved settings.', MSG_TYPE.SUCCESS);
        var setup = res.data;
        if (res.success) {
            $('#setup_id').val(setup.id);
            $('#smtp-server').val(setup.smtpServer);
            $('#smtp-port').val(setup.port);
            $('#smtp-email').val(setup.email);
            $('#smtp-password').val(setup.password);
            $('#default-email').val(setup.defaultEmail);
            $('#default-text').val(setup.defaultMobile);
        }
    });
});