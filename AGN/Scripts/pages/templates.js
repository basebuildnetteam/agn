﻿$(function () {
    var pageInfo = getPageInfo();

    $('#new-template,#btnsave').hide();

    $('#email-tpl-container')
        .on('click', '#back', function () {
            location.href = '/dashboard';
        })
        .on('templates-list-received', '#templates', function (e, load) {
            $(this).html('<option value="">Choose Template</option>');
            load.data.forEach(function (d) {
                $('#templates').append('<option value="'+d.id+'">'+d.name+'</option>');
            });
        })
        .on('click', '#btnedit', function () {
            var list = $('#templates');
            if(list.val().length > 0){
                location.href = '/email/templates/edit/' + list.val();
            }

        })
        .on('click', '#btnnew', function () {
            location.href = '/email/templates/edit';
        })
        .on('click', '#btncopy', function () {
            var list = $('#templates');
            if (list.val().length > 0) {
                $('#new-template,#btnsave').show();
                $('#btnnew').hide();
                $('#new-template').val($('#templates option:selected').text()).attr('data-id', $('#templates').val());
            }
        })
        .on('click', '#btnsave', function () {
            if ($('#new-template').val().length <= 0) {
                $('#new-template').parent().addClass('has-error');
            } else {
                showMessage('Saving new template, please wait...', MSG_TYPE.LOADING);
                ajaxclient.copyEmailTemplate($('#new-template').attr('data-id'), $('#new-template').val())
                    .done(function (res) {
                        if (res.success) {
                            $('#templates').append('<option value="'+res.data.id+'">'+res.data.name+'</option>')
                            showMessage('Successfully copied selected template to "' + $('#new-template').val() + '"', MSG_TYPE.SUCCESS);
                        } else {
                            showMessage('An error has occured while trying to copy template to "' + $('#new-template').val() + '"', MSG_TYPE.ERROR);
                        }
                });
            }
        })

    ;


    $('#text-tpl-container')
        .on('click', '#back', function () {
            location.href = '/dashboard';
        })
        .on('templates-list-received', '#templates', function (e, load) {
            $(this).html('<option value="">Choose Template</option>');
            load.data.forEach(function (d) {
                $('#templates').append('<option value="' + d.id + '">' + d.name + '</option>');
            });
        })
        .on('click', '#btnedit', function () {
            var list = $('#templates');
            if (list.val().length > 0) {
                location.href = '/text/templates/edit/' + list.val();
            }

        })
        .on('click', '#btnnew', function () {
            location.href = '/text/templates/edit';
        })
        .on('click', '#btncopy', function () {
            var list = $('#templates');
            if (list.val().length > 0) {
                $('#new-template,#btnsave').show();
                $('#btnnew').hide();
                $('#new-template').val($('#templates option:selected').text()).attr('data-id', $('#templates').val());
            }
        })
        .on('click', '#btnsave', function () {
            if ($('#new-template').val().length <= 0) {
                $('#new-template').parent().addClass('has-error');
            } else {
                showMessage('Saving new template, please wait...', MSG_TYPE.LOADING);
                ajaxclient.copyTextTemplate($('#new-template').attr('data-id'), $('#new-template').val())
                    .done(function (res) {
                        if (res.success) {
                            $('#templates').append('<option value="' + res.data.id + '">' + res.data.name + '</option>')
                            showMessage('Successfully copied selected template to "' + $('#new-template').val() + '"', MSG_TYPE.SUCCESS);
                        } else {
                            showMessage('An error has occured while trying to copy template to "' + $('#new-template').val() + '"', MSG_TYPE.ERROR);
                        }
                    });
            }
        })


    ;
    if(pageInfo.controller === 'email'){
        ajaxclient.getEmailTemplateList().done(function (res) {
            if (res.success) {
                $('#templates').trigger('templates-list-received', { data: res.data });
            } else {
                console.warn('no templates list received: ' + res.message);
            }
        });
    } else {
        ajaxclient.getTextTemplateList().done(function (res) {
            if (res.success) {
                $('#templates').trigger('templates-list-received', { data: res.data });
            } else {
                console.warn('no templates list received: ' + res.message);
            }
        });
    }



});