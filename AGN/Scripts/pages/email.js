﻿function initEditor() {
    tinymce.init({
        selector: "#content",
        menubar: false,
        height: 100,
        toolbar: "bookmarks | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontsizeselect",
        setup: function (editor) {
            editor.addButton('bookmarks', {
                type: 'menubutton',
                text: 'Bookmarks',
                icon: false,
                menu: [{
                    text: 'Guest First Name',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Guest First Name]') }
                }, {
                    text: 'Guest Last Name',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Guest Last Name]') }
                }, {
                    text: 'Reservation #',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Reservation #]') }
                }, {
                    text: 'Unit Number',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Unit Number]') }
                }, {
                    text: 'Unit Name',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Unit Name]') }
                }, {
                    text: 'Unit Address',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Unit Address]') }
                }, {
                    text: 'Lock Code',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Lock Code]') }
                }, {
                    text: 'Arrival Date',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Arrival Date]') }
                }, {
                    text: 'Departure Date',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Departure Date]') }
                }]
            });
        }
    });
}
$(function () {
    $('#toggle-mobile').hide();
    $('#email-send-container')
        .on('click', '#sendbytext', function () {
            if ($(this).prop('checked')) {
                $('#toggle-mobile').show();
            } else {
                $('#toggle-mobile').hide();
            }
        })
        .on('change', '#sendto', function () {
            if($(this).val() === '1'){
                $('#toggle-res').show();
            } else {
                $('#toggle-res').hide();
            }
        })
        .on('click', '#back,#btncancel', function () { location.href = '/dashboard'; })
        .on('click', '#send-email', function (e) {
            $('#email-form').submit();
        })
        .on('change', '#template', function () {
            if (this.value !== '') {
                var task = this.value.toLowerCase().indexOf('email') > -1 ? ajaxclient.getTemplateDetails(this.value.split('_')[1]) : ajaxclient.getTextTemplateDetails(this.value.split('_')[1]);
                task.done(function (res) {
                    if (res.success) {
                        $('#subject').val(res.data.subject);
                        tinyMCE.activeEditor.setContent(res.data.body);
                    } else {
                        showMessage('Failed to Update Email Template. Please try again.', MSG_TYPE.WARNING);
                    }
                });
            } else {
                $('#subject').val('');
                tinyMCE.activeEditor.setContent('');
            }
        })
        .on('submit', '#email-form', function (e) {
            e.preventDefault();

            var r = confirm("Are you ready to send this email?");
            if(r === true){
                var resNo = $('#reservation-nos');
                var bcc = $('#bcc');
                var subject = $('#subject');
                var content = tinyMCE.activeEditor.getContent();

                if (resNo.val().length <= 0 && $('#sendto').val() === '1') {
                    resNo.parent().addClass('has-error');
                } else if (subject.val().length <= 0) {
                    subject.parent().addClass('has-error');
                } else if (content.length <= 0) {
                    showMessage('Please enter a content to send', MSG_TYPE.WARNING);
                } else {
                    showMessage('Sending message...', MSG_TYPE.LOADING);
                    var data = {
                        sendTo: $('#sendto').val(),
                        emailtemplateid: $('#template').val(),
                        reservationnumbers: resNo.val(),
                        bcc: bcc.val(),
                        subject: subject.val(),
                        body: content,
                        sendFromEmail: $('#from').val(),
                        sendFromText: $('#from-text').val(),
                        sendAsEmail: $('#sendbyemail').prop('checked'),
                        sendAsText: $('#sendbytext').prop('checked')
                    };
                    ajaxclient.sendEmail(data).done(function (res) {
                        if (res.success) {
                            showMessage('Successfully sent email.', MSG_TYPE.SUCCESS);
                        } else {
                            showMessage(res.message, MSG_TYPE.WARNING);
                        }
                    });
                }
            }


        })
    ;


    ajaxclient.getCombineTemplates().done(function (res) {
        if(res.success){
            var list = $('#template').html('<option value="">Choose Template or Leave Blank for Custom</option>');
            res.data.forEach(function (item) {
                list.append('<option value="' + item.id + '">' + item.name + '</option>');
            });
        }
    });

    ajaxclient.getRefSendTo().done(function (res) {
        if (res.success) {
            var list = $('#sendto').html('');
            res.data.forEach(function (item) {
                list.append('<option value="'+item.id+'">'+item.description+'</option>');
            });
        }
    });

    ajaxclient.getSetupDetails().done(function (res) {
        if (res.success) {
            if(res.data){
                $('#from').val(res.data.defaultEmail);
                $('#from-text').val(res.data.defaultMobile);
            }

        }
    });

    initEditor();

});