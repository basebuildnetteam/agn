﻿function fillDropdown() {

    var daysSelected = $('#days').html('').attr('data-selected');
    for (var i = 0; i <= 365; i++) {
        $('#days').append('<option value="' + i + '" '+(i === parseInt(daysSelected)? 'selected':'')+'>' + i + '</option>')
    }

    var resbalance = $('#resbalance').attr('data-selected');
    $('#resbalance option').each(function (index, elem) {
        if (elem.value === resbalance) {
            $(elem).attr('selected', 'selected');
        }
    });

    ajaxclient.getRefWhens().done(function (res) {
        if (res.success) {
            var sel = parseInt($('#refwhen').html('').attr('data-selected'));
            res.data.forEach(function (item) {
                $('#refwhen').append('<option value="' + item.id + '" ' + (item.id === sel ? 'selected' : '') + '>' + item.description + '</option>');
            });
        }
    });
    ajaxclient.getRefEvents().done(function (res) {
        if (res.success) {
            var sel = parseInt($('#refevent').html('').attr('data-selected'));
            res.data.forEach(function (item) {
                $('#refevent').append('<option value="' + item.id + '" ' + (item.id === sel ? 'selected' : '') + '>' + item.description + '</option>');
            });
        }
    });

    ajaxclient.getReservationTypes().done(function (res) {
        if (res.success) {
            var sel = $('#restype').html('').attr('data-selected');
            res.data.forEach(function (item) {
                $('#restype').append('<option value="' + item.id + '" ' + (item.id === sel ? 'selected' : '') + '>' + item.name + '</option>');
            });
        }
    });

    ajaxclient.getReservationGroups().done(function (res) {
        if (res.success) {
            var sel = parseInt($('#resgroups').html('').attr('data-selected'));
            $('#resgroups').html('').append('<option value="0">Any</option>');
            res.data.forEach(function (item) {
                $('#resgroups').append('<option value="' + item.id + '" ' + (item.id === sel ? 'selected' : '') + '>' + item.name + '</option>');
            });
        }
    });
    ajaxclient.getReservationAgents().done(function (res) {
        if (res.success) {
            var sel = parseInt($('#resagent').html('').attr('data-selected'));
            $('#resagent').html('').append('<option value="0">Any</option>');
            res.data.forEach(function (item) {
                $('#resagent').append('<option value="' + item.id + '" ' + (item.id === sel ? 'selected' : '') + '>' + item.name + '</option>');
            });
        }
    });
}
function initEditor() {
    tinymce.init({
        selector: "#content",
        menubar: false,
        height: 100,
        toolbar: "bookmarks | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontsizeselect",
        setup: function (editor) {
            editor.addButton('bookmarks', {
                type: 'menubutton',
                text: 'Bookmarks',
                icon: false,
                menu: [{
                    text: 'Guest First Name',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Guest First Name]') }
                }, {
                    text: 'Guest Last Name',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Guest Last Name]') }
                }, {
                    text: 'Reservation #',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Reservation #]') }
                }, {
                    text: 'Unit Number',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Unit Number]') }
                }, {
                    text: 'Unit Name',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Unit Name]') }
                }, {
                    text: 'Unit Address',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Unit Address]') }
                }, {
                    text: 'Lock Code',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Lock Code]') }
                }, {
                    text: 'Arrival Date',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Arrival Date]') }
                }, {
                    text: 'Departure Date',
                    context: 'bookmarks',
                    onclick: function () { editor.insertContent('[Departure Date]') }
                }]
            });
        }
    });
}

$(function () {
    var pageInfo = getPageInfo();
    $('#email-template-container')
        .on('submit', '#frmsave', function (e) {
            e.preventDefault();
            if ($('#name').val().length <= 0) {
                $('#name').parent().addClass('has-error');
            } else if ($('#subject').val().length <= 0) {
                $('#subject').parent().addClass('has-error');
            } else if (tinyMCE.activeEditor.getContent().length <= 0) {
                showMessage('Email content cannot be empty', MSG_TYPE.WARNING);
            } else {
                showMessage('Please wait while save the template...', MSG_TYPE.LOADING);
                var template = {
                    id: $('#frmsave').attr('data-id'),
                    name: $('#name').val(),
                    senddays: $('#days').val(),
                    refwhen_id: $('#refwhen').val(),
                    refevent_id: $('#refevent').val(),
                    refreservationtype_id: $('#restype').val(),
                    reservationgroup_id: $('#resgroups').val(),
                    rnsbookingagent_id: $('#resagent').val(),
                    fromemail: $('#from').val(),
                    bcc: $('#usebcc').attr('checked'),
                    bccagent: $('#bccagent').attr('checked'),
                    subject: $('#subject').val(),
                    body: tinyMCE.activeEditor.getContent(),
                    balancedue: $('#resbalance').val(),
                }

                ajaxclient.saveEmailTemplate(template).done(function (res) {
                    if(res.success){
                        showMessage('Email Template Successfully saved.', MSG_TYPE.SUCCESS);
                    } else {
                        showMessage('An error has occured while trying to save template', MSG_TYPE.ERROR);
                    }
                });
            }

        })
        .on('change', '#name,#from,#subject', function () {
            if (this.value.length > 0) {
                $(this).parent().removeClass('has-error');
            }
        })
        .on('click', '#back,#btncancel', function () { location.href = '/email/templates'; })
    ;
    if (pageInfo.param2) {

        ajaxclient.getTemplateDetails(pageInfo.param2).done(function (res) {
            if(res.success){
                var item = res.data;
                $('#frmsave').attr('data-id', item.id);
                $('#name').val(item.name);
                $('#days').attr('data-selected', item.days);
                $('#refwhen').attr('data-selected', item.whenid);
                $('#refevent').attr('data-selected', item.eventid);
                $('#restype').attr('data-selected', item.reservationtype);
                $('#resgroups').attr('data-selected', item.reservationgroup);
                $('#resagent').attr('data-selected', item.bookingagent);
                $('#resbalance').attr('data-selected', item.balancedue);
                $('#from').val(item.from);
                $('#usebcc').prop('checked', item.bcc);
                $('#subject').val(item.subject);
                $('#content').val(item.body);

                initEditor();
                fillDropdown();
            }
        });
    } else {
        fillDropdown();
        initEditor();
    }

});