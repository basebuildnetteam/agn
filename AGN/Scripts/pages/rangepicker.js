﻿$(function () {
    $('#report-picker-container')
        .on('click', '.glyphicon', function () {
            $('#' + $(this).attr('data-target-id')).datepicker('show');
        })
        .on('click', '#back', function () {
            location.href = '/reports';
        })
        .on('click', '#btnreport', function () {
            var section = $(this).attr('data-section').toLowerCase();
            var from = $('#dte-from');
            var to = $('#dte-to');

            if (from.val().length <= 0) {
                from.parent().addClass('has-error');
            } else if (to.val().length <= 0) {
                to.parent().addClass('has-error');
            } else {
                location.href = '/reports/details/' + section + '?from=' + encodeURI(from.val()) + '&to=' + encodeURI(to.val());
            }
        })

    ;


    $('#dte-to, #dte-from').datepicker({
        todayHighlight: true,
        clearBtn: true,
        
    })
        .on('changeDate', function (e) {
            if ($(this).val().length > 0)
                $(this).parent().removeClass('has-error');
        })
    ;

});