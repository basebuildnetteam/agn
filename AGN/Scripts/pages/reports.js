﻿var data; 
$(function () {
    $('#reports-container')
        .on('click', '#back', function () {
            location.href = '/dashboard';
        })
        .on('click', 'li', function () {
            var a = $(this).find('a');
            location.href = $(a).attr('href');
        })

    ;

    $('#report-details-container')
        .on('click', '#back', function () {
            location.href = '/reports';
        })

    ;

    $('#reports-detail')
        .on('click', 'span.sorter', function (e) {
            var target = e.target;
            

        })
        .on('data-received', 'tbody')
    ;

    var pageInfo = getPageInfo();
    if (pageInfo.param) {
        showMessage('Please wait, retrieving reports...', MSG_TYPE.LOADING);
        ajaxclient.getReports(pageInfo.param, pageInfo.querystring[0].value, pageInfo.querystring[1].value)
            .done(function (res) {
                showMessage('Successfully retrieved reports.', MSG_TYPE.SUCCESS);
                if (res.success) {
                    data = res.data;

                    var tableBody = $('#reports-detail tbody').html('');
                    res.data.forEach(function (item, index) {
                        var str = '<tr>';
                        str += '<td>' + (index + 1) + '</td>';
                        str += '    <td>' + item.date_sent + '</td>';
                        str += '    <td>' + item.type + '</td>';
                        str += '    <td>' + item.destination + '</td>';
                        str += '</tr>';

                        tableBody.append(str);
                    });
                } else {
                    console.warn('error on data retrieval');
                }
            });
    }


});