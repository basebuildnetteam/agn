﻿"use strict"

var AJAXCLIENT = function () { };
AJAXCLIENT.ENDPOINT = '/';
AJAXCLIENT.REQUEST_METHOD = {
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE',
    PUT: 'PUT',
    HEAD: 'HEAD'
};
AJAXCLIENT.CONTENT_TYPE = {
    JSON: 'application/json',
    X_WWW_FORM_URL_ENCODED: 'application/x-www-form-urlencoded',
};

AJAXCLIENT.prototype = {
    xhrRequest: function (data, mapping, method, cache, contentType, auth, progressClbk) {

        method = (method || AJAXCLIENT.REQUEST_METHOD.GET);
        cache = (cache || false);
        contentType = (contentType || AJAXCLIENT.CONTENT_TYPE.JSON);

        var progressbar = $('#progress-bar');
        var spinner = $('#spinner');
        var def = $.Deferred();

        if(spinner){
            spinner.show();
        }

        var promise = $.ajax({
            cache: cache,
            ifModified: true,
            type: method,
            url: AJAXCLIENT.ENDPOINT + mapping,
            data: data,
            contentType: contentType,
            beforeSend: function (xhr) {
                if (auth !== undefined) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + auth);
                }
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        def.notify(percentComplete * 100);
                    }
                }, false);
                return xhr;
            }
        });

        promise.done(function (response) {
            if (response.message) {
                if (response.message.toLowerCase() === 'session expired') {
                    promptLogin();
                }
            }
            def.resolve(response);
        });

        promise.fail(function (response) {
            def.reject(response);
        });

        if (progressClbk) {
            def.progress(progressClbk);
        } else {
            //default handler is
            def.progress(function (percent) {

                if (progressbar) {
                    console.log(percent);
                    progressbar.css({
                        width: percent + '%',
                    }).show();
                }

            });
        }
        promise.always(function () {
            if (progressbar) {
                progressbar.css({
                    width: 1 + 'px',
                }).show().fadeOut(3000);
            }
            if (spinner) {
                spinner.hide();
            }
        });

        return def.promise();
    },
    getSetupDetails: function () {
        return this.xhrRequest(null, 'setup/details', null, true);
    },
    getEmailTemplateList: function () {
        return this.xhrRequest(null, 'email/templates/list', null, true);
    },
    getTextTemplateList: function () {
        return this.xhrRequest(null, 'text/templates/list', null, true);
    },
    getCombineTemplates: function(){
        return this.xhrRequest(null, 'library/emailandtexttemplates', null, true);
    },
    getRefEvents: function () {
        return this.xhrRequest(null, 'library/refevents', null, true);
    },
    getRefWhens: function () {
        return this.xhrRequest(null, 'library/refwhens', null, true);
    },
    getRefSendTo: function () {
        return this.xhrRequest(null, 'library/sendtos', null, true);
    },
    getReservationTypes: function () {
        return this.xhrRequest(null, 'library/refreservationtypes', null, true);
    },
    getReservationGroups: function () {
        return this.xhrRequest(null, 'library/reservationgroups', null, true);
    },
    getTemplateDetails: function (id) {
        return this.xhrRequest(null, 'email/templates/details/' + id, null, true);
    },
    getTextTemplateDetails: function (id) {
        return this.xhrRequest(null, 'text/templates/details/' + id, null, true);
    },
    getReservationAgents: function () {
        return this.xhrRequest(null, 'library/BookingAgents', null, true);
    },
    getReports: function (section, to, from) {
        if(section == 'email'){
            return this.xhrRequest(null, 'reports/email/list?from=' + from + '&to=' + to, null, true);
        } else {
            return this.xhrRequest(null, 'reports/text/list?from=' + from + '&to=' + to, null, true);
        }

    },
    saveEmailTemplate: function (emailTemplate) {
        return this.xhrRequest(JSON.stringify(emailTemplate), 'email/create', AJAXCLIENT.REQUEST_METHOD.POST,
            false, AJAXCLIENT.CONTENT_TYPE.JSON, null);
    },
    saveTextTemplate: function (textTemplate) {
        return this.xhrRequest(JSON.stringify(textTemplate), 'text/create', AJAXCLIENT.REQUEST_METHOD.POST,
            false, AJAXCLIENT.CONTENT_TYPE.JSON, null);
    },
    copyEmailTemplate: function(id, name){
        return this.xhrRequest('name=' + name , 'email/templates/copy/' + id, AJAXCLIENT.REQUEST_METHOD.POST,
            false, AJAXCLIENT.CONTENT_TYPE.X_WWW_FORM_URL_ENCODED);
    },
    copyTextTemplate: function(id, name){
        return this.xhrRequest('name=' + name , 'text/templates/copy/' + id, AJAXCLIENT.REQUEST_METHOD.POST,
            false, AJAXCLIENT.CONTENT_TYPE.X_WWW_FORM_URL_ENCODED);
    },
    sendEmail: function (data) {
        return this.xhrRequest(JSON.stringify(data), 'email/send', AJAXCLIENT.REQUEST_METHOD.POST,
            false, AJAXCLIENT.CONTENT_TYPE.JSON, null);
    },
    saveSetup: function (setup) {
        return this.xhrRequest(JSON.stringify(setup), 'setup/edit', AJAXCLIENT.REQUEST_METHOD.POST,
            false, AJAXCLIENT.CONTENT_TYPE.JSON, null);
    }
};

var ajaxclient = new AJAXCLIENT();