﻿$('<img src="/Images/cube.gif" />');
var MSG_TYPE = {
    LOADING: 1,
    SUCCESS: 2,
    ERROR: 3,
    WARNING: 4
}

function getPageInfo() {
    var path = window.location.pathname;
    var str = path.split("/");
    return {
        'controller': str[1] === '' ? 'dashboard' : str[1],
        'method': str[2] === undefined ? 'index' : str[2],
        'param': str.length > 3 ? str[3] : undefined,
        'param2': str.length > 4 ? str[4] : undefined,
        'querystring': (function () {
            var arr = [];
            var tmp = location.href.split('?');
            if (tmp.length > 1) {
                var pairs = tmp[1].split('&');
                pairs.forEach(function (pair) {
                    var tmp2 = pair.split('=');
                    arr.push({
                        key: tmp2[0],
                        value: tmp2[1]
                    });
                });
            }
            return arr;
         })()
    };
}

function showMessage(message, type, duration) {

    var alert = $("#alert");
    alert.attr('class','alert');
    switch(type){
        case MSG_TYPE.ERROR:
            duration = duration || 3;
            alert.addClass('alert-danger');
            break;
        case MSG_TYPE.LOADING:
            alert.addClass('alert-info');
            message = '<img id="spinner" src="/Images/cube.gif" />' + message;
            break;
        case MSG_TYPE.SUCCESS:
            alert.addClass('alert-success');
            duration = duration || 3;
            break;
        case MSG_TYPE.WARNING:
            alert.addClass('alert-warning');
            duration = duration || 3;
            break;
    }

    alert.find('#message').html(message);

    alert.slideDown(400, function () {
        if(duration){
            setTimeout(function () {
                alert.closest(".alert").slideUp(400);
            }, duration * 1000);
        }
    });

}

$(function () {
    $("#alert").find(".close").on("click", function (e) {
        e.stopPropagation();   
        e.preventDefault();    
        $(this).closest(".alert").slideUp(400);  
    });

    $('#dashboard-container')
        .on('click', 'li',function () {
            var a = $(this).find('a');
            location.href = $(a).attr('href');
        })
    ;

    $('#alert').width($('.container.body-content').width() + 20);
});