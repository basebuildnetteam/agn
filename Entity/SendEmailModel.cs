﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class SendEmailModel
    {
        public int sendTo { get; set; }
        public string emailtemplateid { get; set; }
        public string reservationnumbers { get; set; }
        public string bcc { get; set; }
        public string subject { get; set; }
        public string body { get; set; }

        public string sendFromEmail { get; set; }
        public string sendFromText { get; set; }
        public bool sendAsEmail { get; set; }
        public bool sendAsText { get; set; }
        //public string stopAlerMessage { get; set; }

    }
}
