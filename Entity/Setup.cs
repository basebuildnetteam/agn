//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Setup
    {
        public int Id { get; set; }
        public string SMTPServer { get; set; }
        public string SMTPEmail { get; set; }
        public Nullable<int> Port { get; set; }
        public string Password { get; set; }
        public string DefaultEmailAddress { get; set; }
        public string DefaultMobileNumber { get; set; }
    }
}
